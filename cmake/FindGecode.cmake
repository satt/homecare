# Try to find the Gecode libraries.
#
# Once done this will define
#
#  GECODE_FOUND - System has Gecode
#  GECODE_INCLUDE_DIR - The Gecode include directories
#  GECODE_LIBRARIES - The libraries needed to use Gecode

message(STATUS "Searching for Gecode")
find_path(GECODE_INCLUDE_DIR 
  NAMES gecode/kernel.hh
  HINTS /usr/local/include /opt/local/include "C:/Program Files/Gecode/include"
)
find_file(GECODE_CONFIG 
  gecode/support/config.hpp
  HINTS /usr/local/include /opt/local/include "C:/Program Files/Gecode/include"
)
## Extract the version
if(GECODE_CONFIG)
  file(STRINGS ${GECODE_CONFIG} GECODE_LINE_VERSION REGEX "^#define GECODE_VERSION .*")
  string(REGEX MATCH "[0-9].[0-9].[0-9]" GECODE_VERSION ${GECODE_LINE_VERSION})
  string(REPLACE "." "-" GECODE_LIBVERSION ${GECODE_VERSION})
endif()
message(STATUS "  version found ${GECODE_VERSION}")

foreach (module support kernel int set driver search minimodel gist flatzinc)
  set(var GECODE_${module}_LIBRARY)
  if (CMAKE_BUILD_TYPE MATCHES "Debug")
    find_library(${var} 
      NAMES gecode${module} gecode${module}-${GECODE_LIBVERSION}-d-x86 gecode${module}-${GECODE_LIBVERSION}-d-x64
      HINTS /usr/local/lib /opt/local/lib "C:/Program Files/Gecode/lib"
    )
  else()
    find_library(${var} 
      NAMES gecode${module} gecode${module}-${GECODE_LIBVERSION}-d-x86 gecode${module}-${GECODE_LIBVERSION}-d-x64
      HINTS /usr/local/lib /opt/local/lib "C:/Program Files/Gecode/lib"
    )
  endif()
  list(APPEND GECODE_LIBRARIES ${${var}})
  list(APPEND GECODE_LIBRARY_VARS ${var})
endforeach ()

if (MSVC)
  link_directories("C:/Program Files/Gecode/lib")
endif()

include(FindPackageHandleStandardArgs)
# Handle the QUIETLY and REQUIRED arguments and set GECODE_FOUND to TRUE
# if all listed variables are TRUE.
find_package_handle_standard_args(Gecode DEFAULT_MSG ${GECODE_LIBRARY_VARS} GECODE_INCLUDE_DIR)

mark_as_advanced(${GECODE_LIBRARY_VARS})
