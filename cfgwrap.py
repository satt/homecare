#!/usr/bin/python
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import collections
from hashlib import md5
import json
import re
import os, sys
import logging
from random import randint
from time import time as now
import subprocess

executable = "./main"

def setup_arguments(parser):

    # arguments
    parser.add_argument("--instance", "-i", required = True, type = str, help = "instance file")
    parser.add_argument("--time", "-t", required = True, type = int, help="solver time limit")
    parser.add_argument("--heuristic", "-he", required = False, default="cp", type = str, help="heuristic to use")
    parser.add_argument("--lns-max-iterations-per-intensity", "-mi", required = False, type=int, help="max iterations per intensity in LNS") 
    parser.add_argument("--lns-time-per-variable", "-tpv", required = False, type=int, help="time per variable in LNS") 

def main():
    
    # setup arguments
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    setup_arguments(parser)    
        
    # parse arguments
    args = parser.parse_args()
    parameters = [executable, "-solutions", 0, "-time", args.time]

    if args.heuristic == "lns":
        parameters.extend(["-heuristic", "lns"])
        parameters.extend(["-lns_time_per_variable", args.lns_time_per_variable])
        parameters.extend(["-lns_max_iterations_per_intensity", args.lns_max_iterations_per_intensity])

    parameters.append(args.instance)

    p = subprocess.Popen(map(str, parameters), stdout=subprocess.PIPE)

    # output matching
    propagations_re = re.compile("\t+propagations:\s+(\d+)")
    nodes_re = re.compile("\t+nodes:\s+(\d+)")
    failures_re = re.compile("\t+failures:\s+(\d+)")
    depth_re = re.compile("\t+peak depth:\s+(\d+)")
    memory_re = re.compile("\t+peak memory:\s+(\d+)")
    n_solutions_re = re.compile("\t+solutions:\s+(\d+)")

    # stats
    best_solution = {}
    cost = 9999999999999999.0 
    propagations = 0
    nodes = 0
    failures = 0
    depth = 0
    memory = 0
    n_solutions = 0
    time = 0
    
    last_best = now()

    while True:

        r = p.stdout.readline()
    
        if r == "":
            break

        try:

            best_solution = json.loads(r) 
            cost = best_solution["cost"]
            time = now() - last_best

        except:

            match = propagations_re.match(r)
            if match != None:
                propagations = float(match.group(1))
            
            match = nodes_re.match(r)
            if match != None:
                nodes = float(match.group(1))
            
            match = failures_re.match(r)
            if match != None:
                failures = float(match.group(1))
            
            match = depth_re.match(r)
            if match != None:
                depth = float(match.group(1))
            
            match = memory_re.match(r)
            if match != None:
                memory = float(match.group(1))
            
            match = n_solutions_re.match(r)
            if match != None:
                n_solutions = float(match.group(1))    

    # to_print = { "cost": cost, "propagations": propagations, "nodes": nodes, "failures": failures, "depth": depth, "memory": memory, "n_solutions": n_solutions, "time": time, "best_solution": best_solution }

    best_solution["propagations"] = int(propagations)
    best_solution["n_solutions"] = int(n_solutions)
    best_solution["failures"] = int(failures)
    best_solution["nodes"] = int(nodes)
    best_solution["depth"] = int(depth)
    best_solution["time_of_best"] = int(time)
    if n_solutions == 0:
        best_solution["cost"] = 9999999999999999.0

    to_print = best_solution

    print json.dumps(to_print, indent=4) 

if __name__ == "__main__":
    main()
