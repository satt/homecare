cmake_minimum_required(VERSION 2.9)

project(HomeCare CXX)

set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${HomeCare_SOURCE_DIR}/cmake)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")

include_directories(${PROJECT_SOURCE_DIR}/json ${PROJECT_SOURCE_DIR}/src ${PROJECT_SOURCE_DIR}/gecode-lns ${PROJECT_SOURCE_DIR}/gecode-aco)

find_package(Boost 1.56.0 COMPONENTS date_time) 

find_package(Gecode REQUIRED)

include_directories(${GECODE_INCLUDE_DIRS})
find_package(Qt5Designer)
find_package(Qt5Core)
find_package(Qt5Widgets)
find_package(Qt5WebKit)
find_package(Qt5WebKitWidgets)
find_package(Qt5Gui)

#add_subdirectory(pugixml-1.2/src)
add_subdirectory(json)
add_subdirectory(gecode-lns)
add_subdirectory(gecode-aco)
add_subdirectory(src)

if (Qt5Widgets_FOUND)	
  add_subdirectory(gui)
endif (Qt5Widgets_FOUND)
