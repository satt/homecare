#include <gecode/int.hh>

// TODO: currently it is based on the assumption that the minimum slack_time for
// all activities is zero
// eventually, slack_times should be added as a vector and proper reasoning on their
// minium values must take place

using namespace Gecode;

class LookaheadRoutingTime : public Propagator
{
protected:
  int d; // the "current" day
  int i; // the "current" activity  
  Int::IntView succ_i;
  Int::IntView departing_time_i;
  Int::IntView duration_succ_i;
  Int::IntView employee_i;
  ViewArray<Int::IntView> employee_start_times;
public:
  // posting
  
  LookaheadRoutingTime(Space& home, int d, int i, Int::IntView, Int::IntView, Int::IntView, Int::IntView, ViewArray<Int::IntView>);
  
  static ExecStatus post(Space& home, int d, int i, Int::IntView, Int::IntView, Int::IntView, Int::IntView, ViewArray<Int::IntView>);  // disposal
  virtual size_t dispose(Space& home); // copying
  
  LookaheadRoutingTime(Space& home, bool share, LookaheadRoutingTime& p);
  
  virtual Propagator* copy(Space& home, bool share);
  
  // cost computation
  virtual PropCost cost(const Space&, const ModEventDelta&) const;
  // propagation
  virtual ExecStatus propagate(Space& home, const ModEventDelta&);
};

void lookahead_routing_time(Space& home, int d, int i, IntVar succ_i, IntVar departing_time_i, IntVar duration_succ_i, IntVar employee_i, IntVarArgs employee_start_times);
