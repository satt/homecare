#include "instance.hpp"
#include <iostream>
#include "cpmodel.hpp"
#include "adapter.hpp"
#include "inspector.hpp"

using namespace Gecode;

template <typename>
class LNSHomeCare : public LNS<BAB, HomeCareModel>
{
public:
  LNSHomeCare(HomeCareModel* s, const Search::Options& o) : LNS<BAB, HomeCareModel>(s, o) {}
};


int main(int argc, char** argv)
{
  HomeCare::LNSHomeCareOptions opt("HomeCare\n");
  opt.parse(argc, argv);
  
  
  if (opt.heuristic() == "cp") {
    
    if (opt.mode() == Gecode::ScriptMode::SM_GIST) {
    
      HomeCare::HomeCareOptions copt("HomeCare");
      copt.parse(argc, argv);
      HomeCare::HomeCareModel* m = new InstantBranchingSpace<HomeCareModel, HomeCareOptions>(copt);
      HomeCare::SolutionInspector v;
      Gist::VarComparator<HomeCare::HomeCareModel> c("Compare nodes");
      Gist::Options o;
      Gist::Print<HomeCare::HomeCareModel> p("Print solution");
      o.inspect.click(&p);
      o.inspect.click(&v);
      o.inspect.compare(&c);
      Gist::bab(m,o);
    
      delete m;
    } else {
      Script::run<InstantBranchingSpace<HomeCareModel, LNSHomeCareOptions>, BAB, LNSHomeCareOptions>(opt);
    }
  }
  else {
    Gecode::Search::Meta::LNS::lns_options = &opt;
    Script::run<HomeCareModel, LNSHomeCare, LNSHomeCareOptions>(opt);
  }
  
  
  
  
  
  return EXIT_SUCCESS;
}
