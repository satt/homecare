#ifndef HOMECARE_solutioninspector_hpp
#define HOMECARE_solutioninspector_hpp

#include "combinedwidgetplugin.h"
#include "schedulingwidget.h"
#include "combinedwidget.h"
#include "adapter.hpp"
#include <QMainWindow>

void init_resources() {
	Q_INIT_RESOURCE(google_maps);
}

namespace HomeCare
{
class SolutionInspector : public Gecode::Gist::Inspector {
public:
  SolutionInspector(QWidget* parent = 0) : initialized(false), parent(parent)
	{}
	
	void init()
	{		
		init_resources();
    aboutDialog = new QMainWindow(parent);
    CombinedWidget* w = new CombinedWidget;
    //SchedulingWidget* w = new SchedulingWidget;
		w->setModel(SchedulingModelAdapter::schedulingItemModel());
		aboutDialog->setCentralWidget(w);		
		aboutDialog->setAttribute(Qt::WA_QuitOnClose, false);
		aboutDialog->setAttribute(Qt::WA_DeleteOnClose, false);
		aboutDialog->resize(640, 480);
		initialized = true;		
	}
	
  ~SolutionInspector()
	{}
	
  virtual void inspect(const Space& s)
  {
    const HomeCareModel& m = static_cast<const HomeCareModel&>(s);
		if (!initialized)
			init();
		SchedulingModelAdapter::updateModel(m);	
    aboutDialog->show();
  }
  
protected:
	bool initialized;
	QWidget* parent;
  QMainWindow *aboutDialog;
};
}
#endif