#ifndef HOMECARE_instance_hpp
#define HOMECARE_instance_hpp
#include <map>
#include <iostream>
#include <set>
#include <vector>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/date_time/gregorian/gregorian.hpp"
#include "json.hh"

namespace HomeCare
{
  using namespace boost::gregorian;
  using namespace boost::posix_time;
  class Item
  {
    friend std::ostream& operator<<(std::ostream& os, const Item& e);
  protected:
    Item(const std::string& id) : id(id) {}
  public:
    typedef std::map<std::string, Item*> ItemMap;
    virtual void Print(std::ostream& os) const = 0;
    static void PrintAll(std::ostream& os);
    virtual void Register()
    {
      items[this->id] = this;
      items_array.push_back(this);
    }
    static Item* _Find(const std::string& id);
    static void Clear()
    { items.clear(); }
    const std::string id;
    static ItemMap items;
    static std::vector<Item*> items_array;
  };
  
  std::ostream& operator<<(std::ostream& os, const Item& e);
  
  template <class TClass, class TInterface>
  class Factory {
  public:
    static void Load(const JSON::Object& doc) { TClass::_Load(doc); }
    static TInterface* Create(const std::string& id)
    {
      TInterface* o = TClass::_Create(id);
      o->Register();
      return o;
    }
    static TClass* Find(const std::string& id)
    {
      return dynamic_cast<TClass*>(TInterface::_Find(id));
    }
    static std::vector<TClass*> AllItems()
    {
      std::vector<TClass*> result;
      for (typename std::vector<TInterface*>::const_iterator it = TInterface::items_array.begin(); it != TInterface::items_array.end(); it++)
      {
        TClass* v = dynamic_cast<TClass*>(*it);
        if (v)
          result.push_back(v);
      }
      return result;
    }
  };
  
  extern time_period null_period;
  
  // TODO: as for timeslots, those not actually used in the working periods should be somehow removed
  class TimeHorizon
  {
  public:
    static void Load(const JSON::Object& doc);
    static time_period horizon;
    static time_duration granularity;
    static std::vector<time_period> working_periods;
    static int timeslots()
    {
      return total_timeslots;
    }
    static int number_of_days()
    {
      return working_periods.size();
    }
    
    static date day(int i)
    {
      // TODO: make some bound checks
      return working_periods[i].begin().date();
    }
  protected:
    // is the total number of (actual) timeslots (i.e., comprised in the business hours of the organization)
    static int total_timeslots;
    // is the accumulated number of timeslots on each day (i.e., the breaking timeslot for the day change)
    static std::vector<int> accumulated_timeslots;
  };
  
  
  int ts(ptime t);
  int ts(time_duration td);
  std::pair<int, int> ts(time_period tp);
  
  ptime pt(int ts);
  time_duration dur(int d);
  
  class Location
  {
  public:
    float latitude, longitude;
    void Print(std::ostream& os) const;
  };
  
  class Activity;
  
  class Employee : public Factory<Employee, Item>, public Item, public Location
  {
    friend class HomeCareInput;
    Employee(const std::string& id) : Item(id) {}
  public:
    void Print(std::ostream& os) const;
    static void _Load(const JSON::Object& doc);
    static Item* _Create(const std::string& id) { return new Employee(id); }
    bool WorksOnDay(int d) const
    {
      if (d > TimeHorizon::number_of_days())
        throw std::logic_error("Days out of bounds");
      return (!daily_worktime_windows[d].is_null());
    }
    std::string name;
    std::vector<time_period> daily_worktime_windows; // one for each day of the planning horizon
    std::pair<int, int> consecutive_working_days; // the minimum and maximum number of working days in the planing orizon
    std::pair<time_duration, time_duration> weekly_worktime; // the total amount of working hours in the planning horizon
    std::vector<time_duration> regular_worktime, max_allowed_worktime; // soft/hard limit for daily working time (for each day)
    std::set<Activity*> compatible_activities;
    // is the total number of employees available on each day
    static std::vector<int> available_employees;
    // is the total number of employee/days in the time horizon (i.e., total sum of the number of persons available on each day)
    static int total_available_employee_days;
  };
  
  class Activity : public Factory<Activity, Item>, public Item, public Location
  {
    friend time_duration distance(const Activity& a1, const Activity& a2);
    friend class Employee;
    friend class HomeCareInput;
    Activity(const std::string& id) : Item(id), allowed_time_window(null_period), duration(minutes(0)) {}
  public:
    void Print(std::ostream& os) const;
    static void _Load(const JSON::Object& doc);
    static Item* _Create(const std::string& id) { return new Activity(id); }
    std::string code;
    std::string name;
    time_period allowed_time_window;
    time_duration duration;
    int number_of_employees;
    std::set<Employee*> compatible_employees;
    // is the total number of (exploded) activities on each day
    static std::vector<int> activities_requirements;
    // is the total number of (exploded) activities in the time horizon
    static int total_activities_requirements;
  };
  
  float distance(const Location* l1, const Location* l2);
  time_duration traveling_time(const Location* l1, const Location* l2);
  
  class HomeCareInput
  {
  public:
    static void LoadInstance(const JSON::Object& doc);
    static void PreprocessInstance();
    static std::vector<std::vector<Activity*> > activity_nodes;
    static std::vector<int> daily_start_regular_node_index, daily_end_regular_node_index;
    static bool is_wildcard(int e) { return e == Employee::AllItems().size() - 1; }
    static int wildcard() { return Employee::AllItems().size() - 1; }
  };
  
} // end of namespace HomeCare

#endif
