#include "adapter.hpp"
#include <algorithm>

namespace HomeCare
{
  SRModel* SchedulingModelAdapter::data_model = 0;
  
  class ActivityPrint : public SRPrintable
  {
  public:
    ActivityPrint(int i, Activity* ac, const IntVar& succ, const IntVar& employee, const IntVar& start_time, const IntVar& duration, const IntVar& slack) : ac(ac)
    {
      std::ostringstream os;
      os << "<strong>Node:</strong> " << i << "<br/>" << std::endl;
      os << "<strong>TimeWindow:</strong> " << ac->allowed_time_window << "<br/>" << std::endl;
      os << "<strong>Employees required:</strong>" << ac->number_of_employees << "<br/>" << std::endl;
      os << "<strong>Succ:</strong> " << succ << "<br/>" << std::endl;
      os << "<strong>Employee:</strong> " << employee << "<br/>" << std::endl;
      os << "<strong>StartTime:</strong> " << start_time << "(ts), " << pt(start_time.min()) << "-" << pt(start_time.max()) << "<br/>" << std::endl;
      os << "<strong>Duration:</strong> " << duration << "(ts), " << dur(duration.min()) << "-" << dur(duration.max()) << "<br/>" << std::endl;
      os << "<strong>Slack:</strong> " << slack << "<br/>" << std::endl;
      os << "<strong>CompatibleEmployees:</strong> ";
      for (const Employee* e : ac->compatible_employees)
      {
        os << e->id << " ";
      }
      os << "<br/>" << std::endl;
      text = QString::fromStdString(os.str());
    }
    virtual QString print() const
    {
      return text;
    }
  protected:
    QString text;
    Activity* ac;
  };

  void SchedulingModelAdapter::updateModel(const HomeCareModel& hc)
  {
    if (!data_model)
      data_model = new SRModel;
    else
      data_model->clear();

    typedef std::vector<Activity*> ActivityList;
    typedef std::vector<Employee*> EmployeeList;

    ActivityList activities = Activity::AllItems();
    EmployeeList employees = Employee::AllItems();

    TimeConstants::granularity = TimeHorizon::granularity;

    std::vector<Grouping*> day_items(TimeHorizon::number_of_days(), 0);
    std::vector<std::vector<SRItem*> > employee_items(TimeHorizon::number_of_days());
    std::vector<std::vector<SRItem*> > activity_items(TimeHorizon::number_of_days());
    Matrix<IntVarArray> weekly_worktime(hc.worktime_on_day_v, TimeHorizon::number_of_days(), employees.size() - 1);
    ptime starting_day = ptime(TimeHorizon::working_periods[0].begin().date());
    for (unsigned int d = 0; d < TimeHorizon::number_of_days(); d++)
    {
      day_items[d] = new Grouping(QString("D%1: %2").arg(d).arg(to_simple_string(TimeHorizon::day(d)).c_str()), TimeHorizon::working_periods[d]);
      time_period daily_worktime = time_period(starting_day + TimeHorizon::working_periods[d].begin().time_of_day(), starting_day + TimeHorizon::working_periods[d].end().time_of_day());
      if (TimeConstants::daily_horizon.is_null())
        TimeConstants::daily_horizon = daily_worktime;
      else
        TimeConstants::daily_horizon = TimeConstants::daily_horizon.span(daily_worktime);
      data_model->addGrouping(day_items[d]);
      Palette p(employees.size() - 1);
      for (EmployeeList::const_iterator it = employees.begin(); it != employees.end(); it++)
      {
        Employee* emp = *it;      
        const unsigned int e = employee_items[d].size();
        SRItem* employee = new SRItem(QString("E%1: %2").arg(e).arg(emp->name.c_str()), emp->daily_worktime_windows[d]);
        employee_items[d].push_back(employee);
        if (hc.start_time[d][e].assigned() && hc.start_time[d][HomeCareInput::daily_end_regular_node_index[d] + e].assigned())
        {
          int departure = hc.start_time[d][e].val();
          int arrival = hc.start_time[d][HomeCareInput::daily_end_regular_node_index[d] + e].val();
          time_period s(pt(departure), dur(arrival - departure));
          employee->addSchedule(s, QColor("lightgray"));
        }

        QColor color;
        if (HomeCareInput::is_wildcard(e))
          color = QColor("black");
        else
        {
          color = p.getColor(e);
          if (!emp->WorksOnDay(d) || (weekly_worktime(d, e).assigned() && weekly_worktime(d, e).val() == 0))
          {
            color = QColor("lightgray");
            color.setAlpha(64);
          }
        }
        data_model->addSRItem(employee, day_items[d], color);
      }
      // first loop for creating all activities and the scheduling part
      Activity* prev_ac = 0;
      unsigned int replica = 0;
      for (unsigned int a = 0; a < HomeCareInput::activity_nodes[d].size(); a++)
      {
        Activity* ac = HomeCareInput::activity_nodes[d][a];
        QString id;
        if (ac->number_of_employees < 2)
        {
          replica = 0;
          prev_ac = 0;
          id = QString("%1").arg(ac->id.c_str());
        }
        else
        {
          if (ac != prev_ac)
          {
            replica = 0;
            prev_ac = ac;
          }
          id = QString("%1/%2").arg(ac->id.c_str()).arg(replica);
          replica++;
        }
        SRItem* activity;
        // FIXME: this will cause a memory leak (since the objects are not deleted)
        ActivityPrint* p = new ActivityPrint(a, ac, hc.succ[d][a], hc.employee[d][a], hc.start_time[d][a], hc.duration[d][a], hc.slack_time[d][a]);
        if (a >= HomeCareInput::daily_start_regular_node_index[d] && a < HomeCareInput::daily_end_regular_node_index[d])
          activity = new SRItem(id, ac->allowed_time_window, ac->latitude, ac->longitude, p);
        else
					activity = new SRItem(id, ac->allowed_time_window, ac->latitude, ac->longitude, p);
        SRItem* employee = 0;
        if (hc.employee[d][a].assigned())
          employee = employee_items[d][hc.employee[d][a].val()];
        if (employee)
          data_model->addSRItem(activity, employee);
        else
          data_model->addSRItem(activity, day_items[d]);
        if (hc.start_time[d][a].assigned() && hc.duration[d][a].assigned())
        {
          time_period s(pt(hc.start_time[d][a].val()), dur(hc.duration[d][a].val()));
          activity->addSchedule(s);
          if (employee)
            employee->addSchedule(s);
          if (hc.slack_time[d][a].assigned() && hc.slack_time[d][a].val() > 0)
          {
            time_period sl(pt(hc.start_time[d][a].val() + hc.duration[d][a].val()), dur(hc.slack_time[d][a].val()));
            activity->addSchedule(sl, QColor("blue"));
            if (employee)
              employee->addSchedule(sl, QColor("blue"));
          }
        }
      }
      // second loop for the routing part
      replica = 0;
      prev_ac = 0;
      for (unsigned int a = 0; a < HomeCareInput::activity_nodes[d].size(); a++)
      {
        Activity* ac = HomeCareInput::activity_nodes[d][a];
        QString id;
        if (ac->number_of_employees < 2)
        {
          replica = 0;
          prev_ac = 0;
          id = QString("%1").arg(ac->id.c_str());
        }
        else
        {
          if (ac != prev_ac)
          {
            replica = 0;
            prev_ac = ac;
          }
          id = QString("%1/%2").arg(ac->id.c_str()).arg(replica);
          replica++;
        }
        if (hc.succ[d][a].assigned() && hc.employee[d][a].assigned() && (hc.succ[d][a].val() < HomeCareInput::daily_end_regular_node_index[d]))
        {
          int succ_a = hc.succ[d][a].val();
          Activity* succ_ac = HomeCareInput::activity_nodes[d][succ_a];
          QString succ_id;
          if (succ_ac->number_of_employees < 2)
            succ_id = QString("%1").arg(succ_ac->id.c_str());
          else
          {
            int i;
            for (i = 0; i < HomeCareInput::activity_nodes[d].size(); i++)
              if (HomeCareInput::activity_nodes[d][i]->id == succ_ac->id)
                break;
            succ_id = QString("%1/%2").arg(succ_ac->id.c_str()).arg(succ_a - i);
          }
          
          SRItem* ai = dynamic_cast<SRItem*>(SRItem::find(id));
          SRItem* succ_ai = dynamic_cast<SRItem*>(SRItem::find(succ_id));
          SRItem* actor = 0;
          actor = employee_items[d][hc.employee[d][a].val()];
          data_model->addRoutingStep(day_items[d], actor, ai, succ_ai);
        }
      }
      for (unsigned int e = 0; e < employees.size() - 1; e++)
      {
        Employee* emp = employees[e];
        SRItem* em = employee_items[d][e];
        if (hc.start_time[d][e].assigned())
        {
          time_period t = time_period(pt(hc.start_time[d][e].val()) + emp->max_allowed_worktime[d], minutes(0));
          em->addSchedule(t, QColor("red"));
        }
      }
    }
  }

}
