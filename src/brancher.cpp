#include "brancher.hpp"
#include "instance.hpp"
#include "cpmodel.hpp"

const bool debug = false;
const bool debug_w = true;

RoutingChoice::RoutingChoice(const RoutingBrancher& b, BranchType t, int d, int a, const std::vector<int>& vals)
: Choice(b, (unsigned int)vals.size()), type(t), day(d), activity(a)
{
  n_values = (int)vals.size();
  values = heap.alloc<int>(n_values);
  for (int i = 0; i < (int)vals.size(); i++)
    values[i] = vals[i];
}


/** Just for serialization */
size_t RoutingChoice::size(void) const
{
  return sizeof(RoutingChoice) + sizeof(int) * n_values;
}

/** Add choice to archive. */

void RoutingChoice::archive(Archive& e) const
{
  Choice::archive(e);
  e << type << day << activity << n_values;
  for (int i = 0; i < n_values; i++)
    e << values[i];
}

/** Free allocated memory */
RoutingChoice::~RoutingChoice()
{
  heap.free<int>(values, n_values);
}

/**
 RoutingBrancher methods definition
 */

std::tuple<int, int, int> RoutingBrancher::next_activity_to_assign(const HomeCareModel& r) const
{
  
  if (debug) std::cerr << "----------------------------------" << std::endl;
  
  typedef std::vector<Employee*> EmployeeList;

  const unsigned int number_of_employees = Employee::AllItems().size();
  const unsigned int days = TimeHorizon::number_of_days();

  // Employee working on day matrix
  EmployeeList employees = Employee::AllItems();
  Matrix<BoolVarArgs> employee_works_on(r.employee_works_on_v, days, number_of_employees - 1);

  for (unsigned int d = 0; d < days; d++)
  {
    if (debug) std::cerr << "Day: " << d << std::endl;
    
    // Index of regular activities on one day
    const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
    
    // Store the lengths of the open paths, to select the shortest one (closed ones are excluded)
    std::vector<std::pair<int, int> > open_paths_length;
    std::vector<int> last_station(number_of_employees);
        
    for (int e = 0; e < number_of_employees; e++)
    {
      int final_depot = regular_activity_end + e;
      
      // Skip employees not working on day d
      if (!HomeCareInput::is_wildcard(e) && employee_works_on(d, e).assigned() && !employee_works_on(d, e).val())
      {
        if (debug) std::cerr << "Employee " << e << " not working on day " << d << std::endl;

        continue;
      } else
        if (debug) std::cerr << "Employee " << e << std::endl;

      // Start from the starting location
      int a = e;
      
      // Length is zero at the beginning
      int length = 0;
      bool closed = false;
      
      // Follow the path to the first unassigned succ activity value in the shortest employee route
      while (r.succ[d][a].assigned() && r.start_time[d][a].assigned())
      {
        if (a == final_depot)
        {
          closed = true;
          break;
        }
        
        length++;
        a = r.succ[d][a].val();
      }
      
      // If the current route has not assigned the start_time first do that
      if (!r.start_time[d][a].assigned())
        return std::make_tuple(d, a, e);
      
      last_station[e] = a;
      
      if (!closed)
        open_paths_length.push_back(std::make_pair(length, e));
      else
        if (debug) std::cerr << "Route closed for employee " << e << std::endl;
    }
    
    if (debug) std::cerr << "Finished computing paths length, open routes " << open_paths_length.size() << "!" << std::endl;
    
    for (unsigned int p = 0; p < open_paths_length.size() && debug; p++)
      std::cerr << open_paths_length[p].second << "(length " << open_paths_length[p].first << ")" << std::endl;
    
    // No open routes, go to next day
    if (open_paths_length.size() == 0)
    {
      if (debug)
      {
        std::cerr << "Succ[" << d << "] = " << r.succ[d] << std::endl;
        std::cerr << "Employee[" << d << "] = " << r.employee[d] << std::endl;
        std::cerr << "Employee working on " << d << " = " << employee_works_on.col(d) << std::endl;
        std::cerr << "Start time[" << d << "] = " << r.start_time[d] << std::endl;
      }
      
      if (debug) std::cerr << "Go to next day" << std::endl;
      continue;
    }
    
    // Wildcard is the only open path
    else if (open_paths_length.size() == 1 && HomeCareInput::is_wildcard(open_paths_length[0].second))
    {
      if (debug) std::cerr << "Fix wildcard employee's path" << std::endl;
      return std::make_tuple(d, last_station[open_paths_length[0].second], open_paths_length[0].second);
    }
    
    // There are open routes
    else
    {
      if (HomeCareInput::is_wildcard(open_paths_length[open_paths_length.size() - 1].second))
        open_paths_length.pop_back();
      // Then prefer the regular employee whose current route is the shortest
      std::sort(open_paths_length.begin(), open_paths_length.end());
      
      if (debug) std::cerr << "Add something to day " << d << " for " << open_paths_length[0].second << std::endl;
      return std::make_tuple(d, last_station[open_paths_length[0].second], open_paths_length[0].second);
    }
  }
  GECODE_NEVER
  return std::make_tuple(0, 0, 0);
}

Choice* RoutingBrancher::choice(Space &home)
{
  const HomeCareModel& r = dynamic_cast<const HomeCareModel&>(home);
  std::vector<int> values(0);
  unsigned int days = TimeHorizon::number_of_days();
  
  // first check whether a still unscheduled activity has its schedule fixed (because it is a replica)
  for (unsigned int d = 0; d < days; d++)
  {
    const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
    const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
    for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
      if (r.start_time[d][a].assigned() && r.duration[d][a].assigned() && !r.employee[d][a].assigned())
      {
        for (Int::ViewValues<Int::IntView> j(r.employee[d][a]); j(); ++j)
          values.push_back(j.val());
        return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_EMPLOYEE, d, a, values);
      }
  }

  // set current station
  std::tuple<int, int, int> day_activity = next_activity_to_assign(r);
  
  int d = std::get<0>(day_activity);
  int a = std::get<1>(day_activity);
  int e = std::get<2>(day_activity);
	// there's a single possibility for the dummy vehicle, that is visit the first remaining unassigned stations
	// this is handled by the commit function
  if (HomeCareInput::is_wildcard(e))
  {
    if (!r.succ[d][a].assigned())
    {
      values.push_back(r.succ[d][a].min());
      return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_SUCC, d, a, values);
    }
    else // !r.start_time[d][a].assigned()
    {
      values.push_back(r.succ[d][a].min());
      return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_START_TIME, d, a, values);
    }
  }
	 
  std::function<RoutingChoice*()> start_time_choice =
  [&]()->RoutingChoice* {
    std::vector<std::pair<int, int> > weighted_values;
    // currently only the minimum value is selected as an alternative
    for (Int::ViewValues<Int::IntView> j(r.start_time[d][a]); j(); ++j)
    {
      // TODO: possibly use a heuristic
      int weight = j.val();
      weighted_values.push_back(std::make_pair(weight, j.val()));
      break;
    }
    std::sort(weighted_values.begin(), weighted_values.end());
    for (unsigned int i = 0; i < weighted_values.size(); i++)
      values.push_back(weighted_values[i].second);
    
    return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_START_TIME, d, a, values);
  };
  std::function<RoutingChoice*()> succ_choice =
  [&]()->RoutingChoice* {
    //std::cerr << "Fixing succ for activity " << a << " in day " << d << std::endl;
		// this case arises when the succ of the current activity of the employee has not been set yet
		std::vector<std::pair<HomeCare::Activity*, int> > weighted_values;
		//int e = r.employee[d][a].val();
    std::vector<HomeCare::Activity*> activities = HomeCareInput::activity_nodes[d];
		for (Int::ViewValues<Int::IntView> j(r.succ[d][a]); j(); ++j)
		{
      // TODO: possibly use a heuristic
			weighted_values.push_back(std::make_pair(activities[j.val()], j.val()));
		}
    std::sort(weighted_values.begin(), weighted_values.end(), [&](std::pair<HomeCare::Activity*, int> i1, std::pair<HomeCare::Activity*, int> i2) {
      HomeCare::Activity *ac1 = i1.first, *ac2 = i2.first;
      int a1 = i1.second, a2 = i2.second;
      // the ending location is always not preferred
      if (ac1->number_of_employees == 0 && ac2->number_of_employees > 0)
        return false;
      if (ac1->number_of_employees > 0 && ac2->number_of_employees == 0)
        return true;
      // then, prefer an activity that is a replica of an already scheduled activity
      if (r.duration[d][a1].min() > 0 && r.duration[d][a2].min() == 0)
        return true;
      if (r.duration[d][a1].min() == 0 && r.duration[d][a2].min() > 0)
        return false;
      // then, prefer an activity that has to end earlier
      if (r.start_time[d][a1].max() + r.duration[d][a1].max() < r.start_time[d][a2].max() + r.duration[d][a2].max())
        return true;
      if (r.start_time[d][a1].max() + r.duration[d][a1].max() > r.start_time[d][a2].max() + r.duration[d][a2].max())
        return false;
      //      if (ac1->allowed_time_window.end() < ac2->allowed_time_window.end())
      //        return true;
      //      if (ac1->allowed_time_window.end() > ac2->allowed_time_window.end())
      //        return false;
      if (ac1->duration < ac2->duration)
        return true;
      if (ac1->duration > ac2->duration)
        return false;
      if (ac1->allowed_time_window.begin() < ac2->allowed_time_window.begin())
        return true;
      if (ac1->allowed_time_window.begin() > ac2->allowed_time_window.begin())
        return false;
      //  prefer an activity with many employees
      if (ac1->number_of_employees > ac2->number_of_employees)
        return true;
      if (ac1->number_of_employees < ac2->number_of_employees)
        return false;
      return i1.second < i2.second;
    });
    
    for (unsigned int i = 0; i < weighted_values.size(); i++)
    {
      // weighted_values[i].first->Print(std::cerr);
			values.push_back(weighted_values[i].second);
    }
    
		return new RoutingChoice(*this, RoutingChoice::BRANCH_ON_SUCC, d, a, values);
  };
  if (starting_location(a, d))
  {
    if (!r.succ[d][a].assigned())
      return succ_choice();
    else // (!r.start_time[d][a].assigned())
      return start_time_choice();
  }
  else
  {
    if (!r.start_time[d][a].assigned())
      return start_time_choice();
    else // if (!r.succ[d][a].assigned())
      return succ_choice();
  }
}

void RoutingBrancher::print(const Space& home, const Choice&	c, unsigned int a, std::ostream& os) const
{
  const RoutingChoice& rc = static_cast<const RoutingChoice&>(c);

  switch (rc.type)
  {
    case RoutingChoice::BRANCH_ON_SUCC:
      os << "s[" << rc.day << "," << rc.activity << "]=" << rc.values[a];
      break;
    case RoutingChoice::BRANCH_ON_START_TIME:
      os << "t[" << rc.day << "," << rc.activity << "]=" << rc.values[a];
      break;
    case RoutingChoice::BRANCH_ON_EMPLOYEE:
      os << "e[" << rc.day << ", " << rc.activity << "]=" << rc.values[a];
      break;
  }
}


bool RoutingBrancher::status(const Space& home) const
{
  const HomeCareModel& r = dynamic_cast<const HomeCareModel&>(home);
  for (unsigned int d = 0; d < TimeHorizon::number_of_days(); d++)
    if (!r.succ[d].assigned() || !r.start_time[d].assigned() || !r.employee[d].assigned())
      return true;
  
  return false;
}

/** Retrieves a choice from the archive. */

Choice* RoutingBrancher::choice(const Space&, Archive& e)
{
  int day, activity, n_values;
  RoutingChoice::BranchType t;
  int t_as_int;
  std::vector<int> values;
  e >> t_as_int >> day >> activity >> n_values;
  switch (t_as_int)
  {
    case 0: t = RoutingChoice::BRANCH_ON_SUCC; break;
    case 1: t = RoutingChoice::BRANCH_ON_START_TIME; break;
    case 2: t = RoutingChoice::BRANCH_ON_EMPLOYEE; break;
  }
  values.resize(n_values, -1);
  for (int i = 0; i < n_values; i++)
    e >> values[i];
  return new RoutingChoice(*this, t, day, activity, values);
}

/** Constructor. */

RoutingBrancher::RoutingBrancher(Home home)
: Brancher(home)
{}

/** Copy constructor. */

RoutingBrancher::RoutingBrancher(Home home, bool share, RoutingBrancher& b)
: Brancher(home, share, b)
{}

/** Clone support. */

Actor* RoutingBrancher::copy(Space& home, bool share) {
  return new (home) RoutingBrancher(home, share, *this);
}

/** Execute choice. */

ExecStatus RoutingBrancher::commit(Space& home, const Choice& c, unsigned int a)
{
  HomeCareModel& r = dynamic_cast<HomeCareModel&>(home);
  const RoutingChoice& rc = static_cast<const RoutingChoice&>(c);
  switch (rc.type)
  {
    case RoutingChoice::BRANCH_ON_SUCC:
      return me_failed(Int::IntView(r.succ[rc.day][rc.activity]).eq(r, rc.values[a])) ? ES_FAILED : ES_OK;
    case RoutingChoice::BRANCH_ON_START_TIME:
      return me_failed(Int::IntView(r.start_time[rc.day][rc.activity]).eq(r, rc.values[a])) ? ES_FAILED : ES_OK;
    case RoutingChoice::BRANCH_ON_EMPLOYEE:
      return fix_employee(r, rc.activity, rc.day, rc.values[a]);
  }
  GECODE_NEVER;
  return ES_FAILED;
}

ExecStatus RoutingBrancher::fix_employee(HomeCareModel& r, int a, int d, int e) const
{
  const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
  int final_depot = regular_activity_end + e;
  
  // Start from the starting location
  int pred_a = e;
  
  // Follow the path to the first unassigned succ activity value in the employee route
  while (r.succ[d][pred_a].assigned())
  {
    if (pred_a == final_depot)
      return ES_FAILED;
    pred_a = r.succ[d][pred_a].val();
  }
  ExecStatus es = me_failed(Int::IntView(r.succ[d][pred_a]).eq(r, a)) ? ES_FAILED : ES_OK;
  if (es == ES_FAILED)
    return es;
  return me_failed(Int::IntView(r.employee[d][a]).eq(r, e)) ? ES_FAILED : ES_OK;
}

/** Post brancher. */

void RoutingBrancher::post(HomeCareModel& home)
{
  (void) new (home) RoutingBrancher(home);
}


void interleaving_brancher(HomeCareModel& home)
{
  if (home.failed())
    return;
  RoutingBrancher::post(home);
}
