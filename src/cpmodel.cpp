#include "cpmodel.hpp"
#include "instance.hpp"
#include <gecode/minimodel.hh>
#include <gecode/set.hh>
#include <random>
#include "json.hh"
#include "propagator.hpp"

//#define NOPRED 1
//#undef NOPRED

using namespace std;
using namespace JSON;

namespace HomeCare
{

  // more succint helper functions
  bool starting_location(int a, int d)
  {
    return a < HomeCareInput::daily_start_regular_node_index[d];
  }

  bool regular_activity(int a, int d)
  {
    return a >= HomeCareInput::daily_start_regular_node_index[d] && a < HomeCareInput::daily_end_regular_node_index[d];
  }

  bool ending_location(int a, int d)
  {
    return a >= HomeCareInput::daily_end_regular_node_index[d];
  }

  bool wildcard(int e)
  {
    return e == HomeCareInput::is_wildcard(e);
  }

	HomeCareModel::HomeCareModel(const HomeCareOptions& opt) : opt(opt)
	{
		std::string instance(opt.instance());

    JSON::Object doc = parse_file(opt.instance());
    HomeCareInput::LoadInstance(doc);

    typedef std::vector<Employee*> EmployeeList;
    EmployeeList employees = Employee::AllItems();
    typedef std::vector<Activity*> ActivityList;

    const unsigned int days = TimeHorizon::number_of_days();

    /*** Routing variables setup ***/
    succ.resize(days);
    std::vector<IntVarArgs> real_daily_distances_per_activity(days);
    std::vector<IntVarArgs> daily_distances_per_activity(days);
    
    std::vector<IntVarArgs> pred;
    if (!opt.only_essential())
      pred.resize(days);
		employee.resize(days);
    std::vector<BoolVarArgs> wildcard_employee(days);

    // traveled distance is expressed in hm (i.e., 100m)
    daily_traveled_distance = IntVarArray(*this, days, 0, Int::Limits::max);
    IntVarArgs real_daily_traveled_distance(*this, days, 0, Int::Limits::max);
    const unsigned int number_of_employees = employees.size(), number_of_regular_employees = employees.size() - 1;
    
    for (unsigned int d = 0; d < days; d++)
		{
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int number_of_activities = daily_activities.size();
      // succ variables take care of the successor of each node in the routing part
			succ[d] = IntVarArray(*this, number_of_activities, 0, number_of_activities - 1);
			// employee variables take care of the employee who is servicing a node
			employee[d] = IntVarArray(*this, number_of_activities, 0, number_of_employees - 1);
      // wildcard_employee states whther an activity is assigned to the wildcard employee
      wildcard_employee[d] = BoolVarArgs(*this, number_of_activities, 0, 1);
    }
    force_check_status();
    
    /*** Routing constraints ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int number_of_activities = daily_activities.size();
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
#ifndef NOPRED
      // pred variables take care of the predecessor of each node in the routing part
			if (!opt.only_essential())
				pred[d] = IntVarArgs(*this, number_of_activities, 0, number_of_activities - 1);
#endif

      // loop on all the activity nodes
			for (unsigned int a = 0; a < number_of_activities; a++)
			{
#ifndef NOPRED
				if (!opt.only_essential())
				{
					// succ/pred relationship
          //rel(*this, element(succ[d], pred[d][a]) == a, opt.icl());
          //rel(*this, element(pred[d], succ[d][a]) == a, opt.icl());
          channel(*this, succ[d], pred[d], opt.icl());
				}
#endif
				if (starting_location(a, d)) // current_node is the starting location for an employee
				{

					const int e = a, pred_e = (e > 0 ? e - 1 : number_of_employees - 1);
					// the successor of the starting location must be either a regular node or the correct ending location
					rel(*this, succ[d][a] >= regular_activity_start, opt.icl());
					rel(*this, (succ[d][a] < regular_activity_end) || (succ[d][a] == regular_activity_end + e), opt.icl());
#ifndef NOPRED
					if (!opt.only_essential())
						// the predecessor of the starting location must be exactly the ending location of the previous employee (so to close the circuit)
						rel(*this, pred[d][a] == regular_activity_end + pred_e, opt.icl());
#endif
					// the employee assigned to the current_node is the employee whose index is e
					rel(*this, employee[d][a] == e, opt.icl());
					// the employee chain is pushed to the succ of current_node
					rel(*this, element(employee[d], succ[d][a]) == e, opt.icl());
          // set the wildcard employee boolean
          rel(*this, wildcard_employee[d][a] == HomeCareInput::is_wildcard(e), opt.icl());
				}
				else if (regular_activity(a, d)) // current_node is a regular activity node
				{
          // prevent to go back to a starting location
					rel(*this, succ[d][a] >= regular_activity_start, opt.icl());
#ifndef NOPRED
					if (!opt.only_essential())
            // the predecessor could not be an ending location
						rel(*this, pred[d][a] < regular_activity_end, opt.icl());
#endif
					// the employee chain is pushed forward to the succ of current activity
					rel(*this, element(employee[d], succ[d][a]) == employee[d][a], opt.icl());
#ifndef NOPRED
					if (!opt.only_essential())
            // the employee chain is pushed backward to the pred of current activity
						rel(*this, element(employee[d], pred[d][a]) == employee[d][a], opt.icl());
#endif
          // set the wildcard employee boolean
          rel(*this, wildcard_employee[d][a] == (employee[d][a] == HomeCareInput::wildcard()), opt.icl());
				}
				else // current_node is the ending location for an employee
				{
					const unsigned int e = a - regular_activity_end;
					// the successor of the ending location must be the starting location of the next employee on the same day (so to force having a circuit)
					rel(*this, succ[d][a] == ((e + 1) % number_of_employees), opt.icl());
#ifndef NOPRED
					if (!opt.only_essential())
            // the predecessor of the ending location must be a regular node or the correct starting location
						rel(*this, (pred[d][a] < regular_activity_end) || (pred[d][a] == e), opt.icl());
#endif
					// the employee assigned to the current activity is the employee whose index is e
					rel(*this, employee[d][a] == e, opt.icl());
#ifndef NOPRED
					if (!opt.only_essential())
            // the employee chain is pushed to the pred of current_node
						rel(*this, element(employee[d], pred[d][a]) == e, opt.icl());
#endif
          // set the wildcard employee boolean
          rel(*this, wildcard_employee[d][a] == HomeCareInput::is_wildcard(e), opt.icl());
				}
			}
			force_check_status();
      IntArgs distances(number_of_activities * number_of_activities);
      IntArgs traveling_times(number_of_activities * number_of_activities);
      setup_matrices(distances, traveling_times, d);
      
      daily_distances_per_activity[d] = IntVarArgs(*this, number_of_activities, 0, Int::Limits::max);
      
			circuit(*this, distances, succ[d],  daily_distances_per_activity[d], daily_traveled_distance[d], opt.icl());
#ifndef NOPRED
			if (!opt.only_essential())
				circuit(*this, distances, pred[d], daily_traveled_distance[d], opt.icl());
#endif
    }
    force_check_status();
    
    /*** Routing incompatibility constraints ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
      for (unsigned int a1 = regular_activity_start; a1 < regular_activity_end; a1++)
        for (unsigned int a2 = regular_activity_start; a2 < regular_activity_end; a2++)
        {
          if (a1 == a2)
            continue;
          Activity* ac1 = daily_activities[a1], *ac2 = daily_activities[a2];
          if (ac1->allowed_time_window.intersects(ac2->allowed_time_window))
            continue;
          if (ac1->allowed_time_window.begin() > ac2->allowed_time_window.end())
            rel(*this, (!wildcard_employee[d][a1]) >> (succ[d][a1] != a2));
        }
    }

    
    /*** Scheduling variables ***/
		start_time.resize(days);
		duration.resize(days);
		slack_time.resize(days); // these are slack times after an activity has been performed
    std::vector<IntVarArgs> departing_time(days); // slack time is assumed to be before departure
    for (unsigned int d = 0; d < days; d++)
		{
      const unsigned int number_of_activities = HomeCareInput::activity_nodes[d].size();
			// start_time variables take care of the starting time and duration of an activity
			start_time[d] = IntVarArray(*this, number_of_activities, ts(TimeHorizon::working_periods[d].begin()), ts(TimeHorizon::working_periods[d].end()));
			// duration is the time lenght of an activity
			duration[d] = IntVarArray(*this, number_of_activities, 0, ts(TimeHorizon::working_periods[d].length()));
			// slack variables
			slack_time[d] = IntVarArray(*this, number_of_activities, 0, ts(TimeHorizon::working_periods[d].length()));
      departing_time[d] = IntVarArgs(*this, number_of_activities, ts(TimeHorizon::working_periods[d].begin()), ts(TimeHorizon::working_periods[d].end()));
      for (unsigned int a = 0; a < number_of_activities; a++)
        departing_time[d][a] = expr(*this, start_time[d][a] + duration[d][a] + slack_time[d][a]);
    }
    force_check_status();

    /*** Scheduling constraints ***/
    for (unsigned int d = 0; d < days; d++)
		{
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int number_of_activities = daily_activities.size();
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
      IntArgs distances(number_of_activities * number_of_activities);
      IntArgs traveling_times(number_of_activities * number_of_activities);
      setup_matrices(distances, traveling_times, d);
      Matrix<IntArgs> m_traveling_times(traveling_times, number_of_activities, number_of_activities);
      
			// loop on all the activity nodes
			for (unsigned int a = 0; a < number_of_activities; a++)
			{
        int start_time_lb = ts(daily_activities[a]->allowed_time_window.begin());
        int start_time_ub = ts(daily_activities[a]->allowed_time_window.end() - daily_activities[a]->duration);
				if (starting_location(a, d)) // current_node is the starting location for an employee
				{
					const int e = a;
          Employee* em = employees[e];
					// the duration of the activity at the starting location is 0
					rel(*this, duration[d][a] == 0, opt.icl());
          if (em->WorksOnDay(d))
            // the employee is working on day d, so his/her time window is set
            dom(*this, start_time[d][a], start_time_lb, start_time_ub);
          else
            // the employee is not working on day d, so a dummy time window is set (i.e., the singleton start time of the working period on that day)
            dom(*this, start_time[d][a], ts(TimeHorizon::working_periods[d].begin()), ts(TimeHorizon::working_periods[d].begin())); // also for symmetry breaking
				}
				else if (regular_activity(a, d)) // current_node is a regular activity
				{
          Activity* ac = daily_activities[a];
					// the starting time of a regular activity must be bounded by its time window
          int allowed_durations[2] = { 0, ts(ac->duration) };
          dom(*this, duration[d][a], IntSet(allowed_durations, 2));
          rel(*this, start_time[d][a] <= start_time_ub);
          // if the activity is assigned to a regular employee, its execution time is bounded by its time window and duration is set to its corresponding value
          rel(*this, (!wildcard_employee[d][a]) == (start_time[d][a] >= start_time_lb && start_time[d][a] <= start_time_ub && duration[d][a] == ts(ac->duration)), opt.icl());
          // otherwise, the start time is fixed to the beginning of the day, and the duration is set to zero (according to the rules of the wildcard employee route)
          rel(*this, wildcard_employee[d][a] >> (start_time[d][a] == ts(TimeHorizon::working_periods[d].begin())), opt.icl());
          rel(*this, wildcard_employee[d][a] == (duration[d][a] == 0), opt.icl());
          rel(*this, wildcard_employee[d][a] >> (slack_time[d][a] == 0), opt.icl());
          // bound also the start_time and departing_time of an activity to the employee time window
          for (unsigned int e = 0; e < number_of_regular_employees; e++)
          {
            Employee* em = employees[e];
            if (em->WorksOnDay(d))
            {
              rel(*this, (employee[d][a] == e) >> (start_time[d][a] >= ts(em->daily_worktime_windows[d].begin()) && departing_time[d][a] <= ts(em->daily_worktime_windows[d].end())), opt.icl());
            }
          }
				}
				else // current_node is the ending location for an employee
				{
					const unsigned int e = a - regular_activity_end;
          Employee* em = employees[e];
					// the duration of the activity at the ending location is 0
					rel(*this, duration[d][a] == 0, opt.icl());
          if (em->WorksOnDay(d))
            // the employee is working on day d, so his/her time window is set
            dom(*this, start_time[d][a], start_time_lb, start_time_ub);
          else
            // the employee is not working on day d, so a dummy time window is set (i.e., the singleton start time of the working period on that day)
            dom(*this, start_time[d][a], ts(TimeHorizon::working_periods[d].begin()), ts(TimeHorizon::working_periods[d].begin()));
				}
			}
			force_check_status();

			// setting up time chain
			for (unsigned int a = 0; a < number_of_activities; a++)
			{
				if (starting_location(a, d))
				{
          int e = a;
					IntVar tt(*this, 0, Int::Limits::max);
					element(*this, m_traveling_times, IntVar(*this, a, a), succ[d][a], tt, opt.icl());
          if (!HomeCareInput::is_wildcard(e))
          {
            BoolVar regular_time = expr(*this, succ[d][a] != regular_activity_end + e);
            // the starting time of a successor activity must be after the end of the current one possibly plus the slack time
            // plus the traveling time, but only in the case of a regular_employee, otherwise it must coincide
            rel(*this, element(start_time[d], succ[d][a]) == start_time[d][a] + regular_time * (duration[d][a] + slack_time[d][a] + tt), opt.icl());
          }
          else
          {
            rel(*this, element(start_time[d], succ[d][a]) == start_time[d][a]);
          }
				}
        if (regular_activity(a, d))
        {
          IntVar tt(*this, 0, Int::Limits::max);
					element(*this, m_traveling_times, IntVar(*this, a, a), succ[d][a], tt, opt.icl());
          BoolVar regular_time = expr(*this, !wildcard_employee[d][a]);
          // the starting time of a successor activity must be after the end of the current one possibly plus the slack time
          // plus the traveling time, but only in the case of a regular_employee, otherwise it must coincide
					rel(*this, element(start_time[d], succ[d][a]) == start_time[d][a] + regular_time * (duration[d][a] + slack_time[d][a] + tt), opt.icl());
        }
        force_check_status();
				if (starting_location(a, d) || ending_location(a, d))
          // no slack time at starting and ending locations
					rel(*this, slack_time[d][a] == 0);

#ifndef NOPRED
				if (!opt.only_essential() && ending_location(a, d))
        {
          int e = a - regular_activity_end;
          IntVar tp(*this, 0, Int::Limits::max);
          BoolVar regular_time = expr(*this, BoolVar(*this, HomeCareInput::is_wildcard(e), HomeCareInput::is_wildcard(e)) && pred[d][a] != e);
          //element(*this, m_traveling_times, pred[d][a], IntVar(*this, a, a), tp, opt.icl());
          //rel(*this, start_time[d][a] == element(start_time[d], pred[d][a]) + regular_time * (element(duration[d], pred[d][a]) + element(slack_time[d], pred[d][a]) + tp), opt.icl());
        }
#endif
        force_check_status();
        if (!opt.only_essential() && regular_activity(a, d))
        {
          IntVar tp(*this, 0, Int::Limits::max);
          BoolVar regular_time = expr(*this, !wildcard_employee[d][a]);
//          element(*this, m_traveling_times, pred[d][a], IntVar(*this, a, a), tp, opt.icl());
//          rel(*this, start_time[d][a] == element(start_time[d], pred[d][a]) + regular_time * (element(duration[d], pred[d][a]) + element(slack_time[d], pred[d][a]) + tp), opt.icl());
        }
			}
      force_check_status();
    }
    
    /** push incompatibility successors constraints **/
    for (unsigned int d = 0; d < days; d++)
    {
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
      for (unsigned int a1 = regular_activity_start; a1 < regular_activity_end; a1++)
        for (unsigned int a2 = regular_activity_start; a2 < regular_activity_end; a2++)
        {
          if (a1 == a2)
            continue;
          rel(*this, (!wildcard_employee[d][a1] && start_time[d][a1] > start_time[d][a2] + duration[d][a2]) >> (succ[d][a1] != a2));
        }
    }

    /*** Synchronization constraints for multi-operator activities ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];

			// impose synchronized start times for activities requiring more than operator
      unsigned int a = regular_activity_start;
			while (a < regular_activity_end - 1)
			{
        int replicas = 1;
				Activity* ac1 = daily_activities[a];
				Activity* ac2 = 0;

				while ((ac2 = daily_activities[a + replicas]) == ac1 && regular_activity(a + replicas, d))
					replicas++;

				if (replicas > 1)
				{
					IntVarArgs start_time_of_replicas, employees_of_replicas, durations_of_replicas;
					for (unsigned int i = 0; i < replicas; i++)
					{
						start_time_of_replicas << start_time[d][a + i];
						employees_of_replicas << employee[d][a + i];
            durations_of_replicas << duration[d][a + i];
            // if on a regular employee, then succ of a replica cannot be another replica
            for (unsigned int j = i + 1; j < replicas; j++)
              if (i != j)
              {
                rel(*this, (wildcard_employee[d][a + i]) == (wildcard_employee[d][a + j]), opt.icl());
                rel(*this, (!wildcard_employee[d][a + i]) >> (employee[d][a + i] != employee[d][a + j]), opt.icl());
                rel(*this, (!wildcard_employee[d][a + i]) >> (!wildcard_employee[d][a + j] && succ[d][a + i] != a + j), opt.icl());
                rel(*this, (!wildcard_employee[d][a + j]) >> (!wildcard_employee[d][a + i] && succ[d][a + j] != a + i), opt.icl());
              }
					}
          int allowed_values[] = { 0, replicas };
          IntVar num_regular_employees_of_replicas(*this, IntSet(allowed_values, 2));
          count(*this, employees_of_replicas, IntSet(0, number_of_regular_employees - 1), IRT_EQ, num_regular_employees_of_replicas, opt.icl());

          for (unsigned int i = 0; i < replicas; i++)
            rel(*this, (num_regular_employees_of_replicas == 0) == (wildcard_employee[d][a + i]), opt.icl());

					nvalues(*this, start_time_of_replicas, IRT_EQ, 1, opt.icl());
          nvalues(*this, durations_of_replicas, IRT_EQ, 1, opt.icl());
				}
				a += replicas;
			}
			force_check_status();
    }
    
    
    /*** Employee (in)compatibility constraints ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
			// avoid incompatible employees
			for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
			{
				Activity* ac = daily_activities[a];
				for (unsigned int e = 0; e < number_of_regular_employees; e++)
				{
					if (ac->compatible_employees.find(employees[e]) == ac->compatible_employees.end())
						rel(*this, employee[d][a] != e, opt.icl());
				}
			}
			force_check_status();
    }
    
    /*** Lookahead succ **/
    // FIXME: temporarily disabled
    for (unsigned int d = 0; d < days; d++)
    {
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
			// avoid incompatible employees
			for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
      {
        IntVar duration_succ = expr(*this, element(duration[d], succ[d][a]));
        lookahead_routing_time(*this, d, a, succ[d][a], departing_time[d][a], duration_succ, employee[d][a], start_time[d].slice(0, 1, regular_activity_end - 1));
      }
      force_check_status();
    }
    
    /*** Employee no-overlap of activities constraints ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
			const unsigned int number_of_regular_activites = regular_activity_end - regular_activity_start;

			// apply no-overlapping constraint on activity schedules (only on regular employees)
			for (unsigned int e = 0; e < number_of_regular_employees; e++)
			{
				BoolVarArgs employee_performs_activity(*this, number_of_regular_activites, 0, 1);
				for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
					rel(*this, employee_performs_activity[a - regular_activity_start] == (employee[d][a] == e), opt.icl());

        // the following variables are meaningless in our case, but they are needed by the unary constraint
        IntVarArgs slacked_durations(*this, number_of_regular_activites, 0, ts(TimeHorizon::working_periods[d].length()));
				unary(*this, start_time[d].slice(regular_activity_start, 1, number_of_regular_activites), slacked_durations, departing_time[d].slice(regular_activity_start, 1, number_of_regular_activites), employee_performs_activity, opt.icl());
			}
      force_check_status();
      
      // If two regular activities are performed by the same employee, then they must start at different times and viceversa
      
//      for(unsigned int a1 = regular_activity_start; a1 < regular_activity_end; a1++)
//        for(unsigned int a2 = regular_activity_start; a2 < regular_activity_end; a2++)
//        {
//          if (a1 == a2)
//            continue;
//          rel(*this, (employee[d][a1] == employee[d][a2] && employee[d][a1] != HomeCareInput::wildcard()) >> (start_time[d][a1] != start_time[d][a2]));
//          rel(*this, (start_time[d][a1] == start_time[d][a2]) >> (employee[d][a1] != employee[d][a2]));
//        }
      force_check_status();
    }

    /*** Daily working times variables ***/
    worktime_on_day_v = IntVarArray(*this, days * number_of_regular_employees, 0, Int::Limits::max);
		Matrix<IntVarArgs> worktime_on_day(worktime_on_day_v, days, number_of_regular_employees);
		employee_works_on_v = BoolVarArray(*this, days * number_of_regular_employees, 0, 1);
		Matrix<BoolVarArgs> employee_works_on(employee_works_on_v, days, number_of_regular_employees);
    IntVarArgs overtime_v(*this, days * number_of_regular_employees, 0, Int::Limits::max);
		Matrix<IntVarArgs> overtime(overtime_v, days, number_of_regular_employees);


    /*** Daily working times constraints ***/
    for (unsigned int d = 0; d < days; d++)
    {
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
      const unsigned int number_of_activities = HomeCareInput::activity_nodes[d].size();
      IntArgs distances(number_of_activities * number_of_activities);
      IntArgs traveling_times(number_of_activities * number_of_activities);
      setup_matrices(distances, traveling_times, d);
      Matrix<IntArgs> m_traveling_times(traveling_times, number_of_activities, number_of_activities);
      
      // working times (variables setting and daily limits)
      for (unsigned int e = 0; e < number_of_regular_employees; e++)
      {
        Employee* em = employees[e];
        rel(*this, worktime_on_day(d, e) == start_time[d][regular_activity_end + e] - start_time[d][e], opt.icl());
        force_check_status();
        
        if (opt.working_time())
        {
          // this is the hard bound on the maximum working time in a day
          rel(*this, worktime_on_day(d, e) <= ts(em->max_allowed_worktime[d]), opt.icl());
          for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
          {
            // lookahead going directly to the depot
            IntVar tt(*this, 0, Int::Limits::max);
            element(*this, m_traveling_times, IntVar(*this, a, a), IntVar(*this, regular_activity_end + e, regular_activity_end + e), tt, opt.icl());
            rel(*this, (employee[d][a] == e) >> (departing_time[d][a] + tt - start_time[d][e] <= ts(em->max_allowed_worktime[d])), opt.icl());
          }
          force_check_status();
          // compute overtime
          rel(*this, overtime(d, e) == max(0, worktime_on_day(d, e) - ts(em->regular_worktime[d])), opt.icl());
          if (!opt.flexible_working_time())
            // this is a hard bound on the minimum working time in a day
            rel(*this, worktime_on_day(d, e) == 0 || worktime_on_day(d, e) >= ts(em->regular_worktime[d]), opt.icl());
          force_check_status();
        }
      }
      force_check_status();
      // for the wildcard employee
      rel(*this, start_time[d][HomeCareInput::wildcard()] == ts(TimeHorizon::working_periods[d].begin()), opt.icl());
    }
    force_check_status();
    
    /*** Working days constraints ***/
    for (unsigned int d = 0; d < days; d++)
      for (unsigned int e = 0; e < number_of_regular_employees; e++)
      {
        rel(*this, employee_works_on(d, e) == (worktime_on_day(d, e) > 0), opt.icl());
        
        // Worktime is zero for employees not working (to cope with brancher)
        rel(*this, !employee_works_on(d, e) == (worktime_on_day(d,e) == 0), opt.icl());
      }
    force_check_status();
    /*** Explicit pruning of non working employee for a day ***/
    if (!opt.only_essential())
    {
      for (unsigned int d = 0; d < days; d++)
      {
        const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
        const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];

        for (unsigned int e = 0; e < number_of_regular_employees; e++)
        {
          for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
            rel(*this, (!employee_works_on(d, e)) >> (employee[d][a] != e), opt.icl());
        }
      }
    }
    force_check_status();
    
    if (opt.working_time())
    {
      /*** Horizon working time and overtime contraints ***/
      if (!opt.flexible_working_time())
      {
        for (unsigned int e = 0; e < number_of_regular_employees; e++)
        {
          Employee* em = employees[e];
          // constraints on overtime
          // FIXME: temporarily disabled
          rel(*this, sum(overtime.row(e)) <= ts(em->weekly_worktime.second - em->weekly_worktime.first), opt.icl());
          // constraints on total worktime in the horizon
          IntVar worktime_in_horizon(*this, ts(em->weekly_worktime.first), ts(em->weekly_worktime.second));
          rel(*this, worktime_in_horizon == sum(worktime_on_day.row(e)), opt.icl());
        }
      }
      force_check_status();

      // constraints on sequences of working days
      for (unsigned int e = 0; e < number_of_regular_employees; e++)
      {
        Employee* emp = employees[e];
        // in the sequence of #days there should be a number of consecutive working days between the two bounds (minimum and maximum)
        sequence(*this, employee_works_on.row(e), IntSet(1, 1), days, emp->consecutive_working_days.first, emp->consecutive_working_days.second, opt.icl());
      }
      force_check_status();

      number_of_employees_working_on_day = IntVarArray(*this, days, 1, number_of_regular_employees);
      for (unsigned int d = 0; d < days; d++)
        rel(*this, number_of_employees_working_on_day[d] == sum(employee_works_on.col(d)), opt.icl());

      if (!opt.only_essential())
      {
        for (unsigned int d = 0; d < days; d++)
        {
          // go straight to the ending location if the employee does not work on that day
          const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
          const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
          
          for (unsigned int e = 0; e < number_of_regular_employees; e++)
          {
            rel(*this, (employee_works_on(d, e) == 0) == (succ[d][e] == regular_activity_end + e), opt.icl());
#ifndef NOPRED
            rel(*this, (employee_works_on(d, e) == 0) == (pred[d][regular_activity_end + e] == e), opt.icl());
#endif
            
            // if the employee doesn't work one a day, its total worktime on that day must be zero
            rel(*this, (employee_works_on(d, e) == 0) == (worktime_on_day(d, e) == 0), opt.icl());
            
            // also, no activity in that day can be assigned to her
            for (unsigned int a = regular_activity_start; a < regular_activity_end; a++)
              rel(*this, (employee_works_on(d,e) == 0) >> (employee[d][a] != e), opt.icl());
          }
        }
      }
      force_check_status();
    }
    else
      // no overtime in case of no constraints on working_time
      linear(*this, overtime_v, IRT_EQ, 0, opt.icl());
    

    // accumulate the slacktime per day
		IntVarArgs daily_slack_time(*this, days, 0, Int::Limits::max);
		for (unsigned int d = 0; d < days; d++)
			rel(*this, daily_slack_time[d] == sum(slack_time[d]), opt.icl());
    force_check_status();

    // symmetry breaking on start_time on non working days
    if (!opt.only_essential())
    {
      for (unsigned int d = 0; d < days; d++)
      {
        const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
        for (unsigned int e = 0; e < number_of_regular_employees; e++)
        {
          Employee* em = employees[e];
          if (em->WorksOnDay(d))
            rel(*this, (!employee_works_on(d, e)) == (start_time[d][e] == ts(em->daily_worktime_windows[d].begin()) && start_time[d][regular_activity_end + e] == ts(em->daily_worktime_windows[d].begin())), opt.icl());
        }
      }
    }

    IntVarArgs number_of_unassigned_activities(*this, days, 2, Int::Limits::max);
    for (unsigned int d = 0; d < days; d++)
    {
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];

      dom(*this, number_of_unassigned_activities[d], 2, regular_activity_end - regular_activity_start + 2);
      count(*this, employee[d], HomeCareInput::wildcard(), IRT_EQ, number_of_unassigned_activities[d], opt.icl());
      rel(*this, number_of_unassigned_activities[d] == sum(wildcard_employee[d]), opt.icl());
      force_check_status();
    }

    // for branching
    IntVarArgs number_of_employees_working_on_day(*this, days, 1, number_of_regular_employees);
    for (unsigned int d = 0; d < days; d++)
      rel(*this, number_of_employees_working_on_day[d] == sum(employee_works_on.col(d)), opt.icl());

    open_route = std::vector<IntVarArray>(days); // these state whether a route has not been closed (works only for the constructive brancher)
    for (unsigned int d = 0; d < days; d++)
		{
      const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
      const unsigned int regular_activity_start = HomeCareInput::daily_start_regular_node_index[d];

			open_route[d] = IntVarArray(*this, number_of_employees, regular_activity_start - number_of_employees, regular_activity_end);
#ifndef NOPRED
      if (!opt.only_essential())
        for (unsigned int e = 0; e < number_of_employees; e++)
          rel(*this, open_route[d][e] == pred[d][regular_activity_end + e], opt.icl());
#endif
    }
    
    force_check_status();
    
    // Remove distances related to wildcard employee from cost!
    for (unsigned int d = 0; d < days; d++)
    {
      const ActivityList& daily_activities = HomeCareInput::activity_nodes[d];
      const unsigned int number_of_activities = daily_activities.size();
      real_daily_distances_per_activity[d] = IntVarArgs(*this, number_of_activities, 0, Int::Limits::max);
      
      // Temporary
      IntVarArgs not_wildcard_employee(*this, number_of_activities, 0, 1);
      for (unsigned int a = 0; a < number_of_activities; a++)
        rel(*this, (not_wildcard_employee[a] == 1) == (wildcard_employee[d][a] == false));
      
      for (unsigned int a = 0; a < number_of_activities; a++)
        rel(*this, real_daily_distances_per_activity[d][a] == (daily_distances_per_activity[d][a] * (not_wildcard_employee[a])));
      
      rel(*this, real_daily_traveled_distance[d] == sum(real_daily_distances_per_activity[d]));
      
    }

    // For debug reasons distances
		cost_var = IntVar(*this, 0, Int::Limits::max);
		total_traveled_distance = expr(*this, sum(real_daily_traveled_distance));
    
    total_overtime = expr(*this, sum(overtime_v));
    total_slacktime = expr(*this, sum(daily_slack_time));
    total_unassigned_activities = expr(*this, sum(number_of_unassigned_activities) - 2 * days);

    // FIXME: the traveled_distances must be fixed by removing the contribution of the wildcard employee
		// cost of one kilometer, 0.30 Euro, cost of 1 minute work 25 Euro / 60 minutes
		rel(*this, cost_var == total_traveled_distance * 3
        + total_overtime * TimeHorizon::granularity.minutes() * 40
        + total_slacktime * TimeHorizon::granularity.minutes() * 40
        + total_unassigned_activities * 10000, opt.icl()); // on all days there are two dummy unassigned activities (i.e., starting and ending location of wildcard employee)
  
    
		force_check_status();        

  }
  
  void HomeCareModel::initial_solution_branching(unsigned long int restart)
  {
    Rnd r(0U);
    unsigned int days = TimeHorizon::number_of_days();
    unsigned int number_of_regular_employees = Employee::AllItems().size() - 1;
    
    
    Matrix<BoolVarArgs> employee_works_on(employee_works_on_v, days, number_of_regular_employees);
    
    // Branch on number of employees working
    branch(*this, number_of_employees_working_on_day, INT_VAR_NONE(), INT_VAL_MED());
    
    // Other branching variables
		IntVarArgs all_variables;
		for (unsigned int d = 0; d < days; d++)
		  branch(*this, employee_works_on.col(d), INT_VAR_NONE(), INT_VAL_RND(r));

    // Interleaving branchner
    interleaving_brancher(*this);
  }
  
  void HomeCareModel::neighborhood_branching()
  {
    Rnd r;
    r.time(); // only difference, random
    
    unsigned int days = TimeHorizon::number_of_days();
    unsigned int number_of_regular_employees = Employee::AllItems().size() - 1;
    
    
    Matrix<BoolVarArgs> employee_works_on(employee_works_on_v, days, number_of_regular_employees);
    
    // Branch on number of employees working
    branch(*this, number_of_employees_working_on_day, INT_VAR_NONE(), INT_VAL_RND(r));
    
    // Other branching variables
		IntVarArgs all_variables;
		for (unsigned int d = 0; d < days; d++)
		  branch(*this, employee_works_on.col(d), INT_VAR_NONE(), INT_VAL_RND(r));
    
    // Interleaving branchner
    interleaving_brancher(*this);
  }
  
  unsigned int HomeCareModel::relax(Space* that, unsigned int free)
  {
    HomeCareModel* ns = dynamic_cast<HomeCareModel*>(that);
    
    // Maximum number of relaxable days
    Gecode::Search::Meta::LNS::lns_options->maxIntensity(TimeHorizon::number_of_days());
    
    // Actual relaxed days
    std::set<unsigned int> relaxed_days;
    unsigned int free_vars = 0;
    
    // We need to assign all days except one
    unsigned int days = TimeHorizon::number_of_days();
    unsigned int regular_employees = Employee::AllItems().size() - 1;
    
    // Random
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> days_dis(0, days-1);
    uniform_int_distribution<> emp_dis(0, regular_employees-1);
    
    // Choose days to relax
    while (relaxed_days.size() != free)
    {
      unsigned int day_to_relax = days_dis(gen);
      relaxed_days.insert(day_to_relax);
    }
    
    for (unsigned int d = 0; d < days; d++)
    {
      // If day is among the ones to relax
      if (relaxed_days.find(d) != relaxed_days.end())
      {
        free_vars += ns->succ[d].size();
        free_vars += ns->employee[d].size();
        free_vars += ns->start_time[d].size();
        free_vars += ns->duration[d].size();
        free_vars += ns->slack_time[d].size();
      }
      // If day must be fixed
      else
      {
        if (opt.neighborhood() == "two-employees")
        {
          // Pick employee to relax
          std::set<unsigned int> relaxed_employees;

          // If there are unassigned activities, add wildcard
          if (total_unassigned_activities.val() > 0)
            relaxed_employees.insert(HomeCareInput::wildcard());
          
          // Fill vector of relaxed employees with random employees
          while (relaxed_employees.size() < 2)
            relaxed_employees.insert(emp_dis(gen));

          // Cycle through employees not to relax
          for (unsigned int a = 0; a < ns->succ[d].size(); a++)
          {
            // If employee of action is not to relax, fix it
            if (relaxed_employees.find(employee[d][a].val()) == relaxed_employees.end())
            {
              rel(*ns, ns->succ[d][a] == succ[d][a]);
              rel(*ns, ns->employee[d][a] == employee[d][a]);
              rel(*ns, ns->start_time[d][a] == start_time[d][a]);
              rel(*ns, ns->duration[d][a] == duration[d][a]);
              rel(*ns, ns->slack_time[d][a] == slack_time[d][a]);
            }
            else
            {
              free_vars += 5;
            }
          }
        }
        else
        {
          for (unsigned int a = 0; a < ns->succ[d].size(); a++)
          {
            rel(*ns, ns->succ[d][a] == succ[d][a]);
            rel(*ns, ns->employee[d][a] == employee[d][a]);
            rel(*ns, ns->start_time[d][a] == start_time[d][a]);
            rel(*ns, ns->duration[d][a] == duration[d][a]);
            rel(*ns, ns->slack_time[d][a] == slack_time[d][a]);
          }
        }
      }
    }
    
    // std::cerr << "Print free vars " << free_vars << std::endl;
    
    return free_vars;
  }
  
  void HomeCareModel::setup_matrices(IntArgs& distances, IntArgs& times, int d) const
  {
    const unsigned int number_of_activities = HomeCareInput::activity_nodes[d].size();
    Matrix<IntArgs> m_distances(distances, number_of_activities, number_of_activities);
    Matrix<IntArgs> m_times(times, number_of_activities, number_of_activities);
    
		// setting up distances and traveling times
    int start_node, end_node;
		start_node = 0;
    end_node = HomeCareInput::activity_nodes[d].size();
		for (unsigned int current_node = start_node; current_node < end_node; current_node++)
		{
			m_distances(current_node, current_node) = 0;
			m_times(current_node, current_node) = 0;
			for (unsigned int other_node = current_node + 1; other_node < end_node; other_node++)
			{
				// forth
				m_distances(current_node, other_node) = round(distance(dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][current_node]), dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][other_node])) * 10);
				time_duration tt = traveling_time(dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][current_node]), dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][other_node]));
				m_times(current_node, other_node) = ts(tt);
				// back
				m_distances(other_node, current_node) = round(distance(dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][other_node]), dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][current_node])) * 10);
				tt = traveling_time(dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][other_node]), dynamic_cast<Location*>(HomeCareInput::activity_nodes[d][current_node]));
				m_times(other_node, current_node) = ts(tt);
			}
		}
    distances = m_distances.get_array();
    times = m_times.get_array();
  }

	/** Copy constructor */
	HomeCareModel::HomeCareModel(bool share, HomeCareModel& s) : LNSMinimizeScript(share, s), opt(s.opt)
	{
    
    const unsigned int days = TimeHorizon::number_of_days();
		succ.resize(days);
		employee.resize(days);
		start_time.resize(days);
		duration.resize(days);
		slack_time.resize(days);
		open_route.resize(days);
    
		for (unsigned int d = 0; d < days; d++)
		{
			succ[d].update(*this, share, s.succ[d]);
			employee[d].update(*this, share, s.employee[d]);
			start_time[d].update(*this, share, s.start_time[d]);
			duration[d].update(*this, share, s.duration[d]);
			slack_time[d].update(*this, share, s.slack_time[d]);
			open_route[d].update(*this, share, s.open_route[d]);
    }
    
		worktime_on_day_v.update(*this, share, s.worktime_on_day_v);
    employee_works_on_v.update(*this, share, s.employee_works_on_v);
		cost_var.update(*this, share, s.cost_var);
    total_traveled_distance.update(*this, share, s.total_traveled_distance);
    total_overtime.update(*this, share, s.total_overtime);
    total_slacktime.update(*this, share, s.total_slacktime);
    daily_traveled_distance.update(*this, share, s.daily_traveled_distance);
    total_unassigned_activities.update(*this, share, s.total_unassigned_activities);
    number_of_employees_working_on_day.update(*this, share, s.number_of_employees_working_on_day);
	}

	/** Clonation */
	Space* HomeCareModel::copy(bool share)
	{
		return new HomeCareModel(share, *this);
	}

	/** Cost variable */
	IntVar HomeCareModel::cost() const
	{
		return cost_var;
	}

	/** Printing */
	void HomeCareModel::print(std::ostream& os) const
	{

    os << "{ ";
    
    // cost
    os << "\"cost\": " << cost_var;
    os << ", \"cost_unassigned\": " << total_unassigned_activities;
    os << ", \"cost_overtime\": " << total_overtime;
    os << ", \"cost_slack\": " << total_slacktime;
    os << ", \"cost_distance\": " << total_traveled_distance;
    os << " }" << endl;

    
    
    /*
		Matrix<IntVarArgs> worktime_on_day(worktime_on_day_v, days, Employee::AllItems().size() - 1);
		for (unsigned int d = 0; d < days; d++)
		{
			os << "Succ[" << d << "]: " << succ[d] << std::endl;
			os << "Empl[" << d << "]: " << employee[d] << std::endl;
			os << "ST[" << d << "]:" <<  start_time[d] << std::endl;
			//os << "STt[" << d << "]";
			//for (unsigned int i = 0; i < start_time[d].size(); i++)
			//  os << time_period(TimeHorizon::ptime_from_timeslot(start_time[d][i].min()), TimeHorizon::timeslots_to_duration(start_time[d][i].max() - start_time[d][i].min())) << " ";
			//os << std::endl; 
			os << "D[" << d << "]:"  << duration[d] << std::endl;
			os << "SL[" << d << "]:" << slack_time[d] << std::endl;
			os << "WT[" << d << "]: " << worktime_on_day.col(d) << std::endl;
		}
		os << "Cost: " << cost_var << std::endl;
    os << "Traveled Distance: " << daily_traveled_distance << " (total: " << total_traveled_distance << ")" << std::endl;
    os << "Overtime: " << total_overtime << std::endl;
    os << "Slacktime: " << total_slacktime << std::endl;
    os << "Unassigned activities: " << total_unassigned_activities << std::endl;
    */
	}
}

