#ifndef HOMECARE_schedulingitemmodeladapter_hpp
#define HOMECARE_schedulingitemmodeladapter_hpp

#include "cpmodel.hpp"
#include "srmodel.h"

namespace HomeCare
{
  class SchedulingModelAdapter
  {
  public:
    static void updateModel(const HomeCareModel& hc);
    static SRModel* schedulingItemModel()
    {
      if (!data_model)
        data_model = new SRModel;
      return data_model;
    }
  protected:
    static SRModel* data_model;
  };
  
}

#endif
