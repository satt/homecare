#include "propagator.hpp"
#include "cpmodel.hpp"

using namespace HomeCare;

LookaheadRoutingTime::LookaheadRoutingTime(Space& home, int d, int i, Int::IntView succ_i, Int::IntView departing_time_i, Int::IntView duration_succ_i, Int::IntView employee_i, ViewArray<Int::IntView> employee_start_times)
: Propagator(home), d(d), i(i), succ_i(succ_i), departing_time_i(departing_time_i), duration_succ_i(duration_succ_i), employee_i(employee_i), employee_start_times(employee_start_times)
{
  departing_time_i.subscribe(home, *this, Int::PC_INT_BND);
  duration_succ_i.subscribe(home, *this, Int::PC_INT_BND);
  employee_i.subscribe(home, *this, Int::PC_INT_DOM);
  employee_start_times.subscribe(home, *this, Int::PC_INT_BND);
}

ExecStatus LookaheadRoutingTime::post(Space& home, int d, int i, Int::IntView succ_i, Int::IntView departing_time_i, Int::IntView duration_succ_i, Int::IntView employee_i, ViewArray<Int::IntView> employee_start_times)
{
  (void) new (home) LookaheadRoutingTime(home, d, i, succ_i, departing_time_i, duration_succ_i, employee_i, employee_start_times);
  return ES_OK;
}

size_t LookaheadRoutingTime::dispose(Space& home)
{
  departing_time_i.cancel(home, *this, Int::PC_INT_BND);
  duration_succ_i.cancel(home, *this, Int::PC_INT_BND);
  employee_i.cancel(home, *this, Int::PC_INT_DOM);
  employee_start_times.cancel(home, *this, Int::PC_INT_BND);
  (void) Propagator::dispose(home);
  return sizeof(*this);
}


LookaheadRoutingTime::LookaheadRoutingTime(Space& home, bool share, LookaheadRoutingTime& p): Propagator(home, share, p), d(p.d), i(p.i)
{
  succ_i.update(home, share, p.succ_i);
  departing_time_i.update(home, share, p.departing_time_i);
  duration_succ_i.update(home, share, p.duration_succ_i);
  employee_i.update(home, share, p.employee_i);
  employee_start_times.update(home, share, p.employee_start_times);
}

Propagator* LookaheadRoutingTime::copy(Space& home, bool share)
{
  return new (home) LookaheadRoutingTime(home, share, *this);
}

// cost computation
PropCost LookaheadRoutingTime::cost(const Space&, const ModEventDelta&) const
{
  // TODO: real cost should be understood
  return PropCost::linear(PropCost::LO, employee_i.size() * succ_i.size()); // we only propagate on succ_i
}

// propagation
ExecStatus LookaheadRoutingTime::propagate(Space& home, const ModEventDelta&)
{
  if (succ_i.assigned())
    return home.ES_SUBSUMED(*this);
  
  if (employee_i.in(HomeCareInput::wildcard()))
    return ES_NOFIX;
  
  const std::vector<HomeCare::Activity*>& daily_activities = HomeCareInput::activity_nodes[d];
  const unsigned int regular_activity_end = HomeCareInput::daily_end_regular_node_index[d];
  const std::vector<HomeCare::Employee*>& employees = Employee::AllItems();
  
  std::vector<int> pruned_values;
  HomeCare::Activity* ac = daily_activities[i];

  for (Int::ViewValues<Int::IntView> j(succ_i); j(); ++j)
  {
    bool depot = false;
    for (Int::ViewValues<Int::IntView> e(employee_i); e(); ++e)
    {
      if (j.val() == ending_location(e.val(), d))
      {
        depot = true;
        break;
      }
    }
    
    if (depot)
      continue;
    
    HomeCare::Activity* succ_ac = daily_activities[j.val()];
    
    // Consider each successor of i in turn (j)
    int minimum_lookahed_time_to_succ = departing_time_i.min() + ts(traveling_time(ac, succ_ac)) + duration_succ_i.min();
    for (Int::ViewValues<Int::IntView> e(employee_i); e(); ++e)
    {
      if (e.val() == HomeCareInput::wildcard())
        continue;
      HomeCare::Activity* employee_depot = daily_activities[regular_activity_end + e.val()];
      HomeCare::Employee* em = employees[e.val()];
      int succ_depot_traveling_time = ts(traveling_time(succ_ac, employee_depot));
      if (minimum_lookahed_time_to_succ + succ_depot_traveling_time - employee_start_times[e.val()].max() > ts(em->max_allowed_worktime[d]))
      {
        pruned_values.push_back(j.val());
        break;
      }
      if (minimum_lookahed_time_to_succ + succ_depot_traveling_time > ts(em->daily_worktime_windows[d].end()))
      {
        pruned_values.push_back(j.val());
        break;        
      }
    }
  }

  for (std::vector<int>::const_iterator it = pruned_values.begin(); it != pruned_values.end(); it++)
  {
		ModEvent me = succ_i.nq(home, *it);
    if (me_failed(me))
      return ES_FAILED;
		if (me == Int::ME_INT_VAL)
			return home.ES_SUBSUMED(*this);
  }
  
	return ES_FIX;
}

void lookahead_routing_time(Space& home, int d, int i, IntVar succ_i, IntVar departing_time_i, IntVar duration_succ_i, IntVar employee_i, IntVarArgs employee_start_times)
{
  // constraint post function
  Int::IntView succ_i_v(succ_i), departing_time_i_v(departing_time_i), duration_succ_i_v(duration_succ_i), employee_i_v(employee_i);
  ViewArray<Int::IntView> employee_start_times_v(home, employee_start_times);
  
  if (LookaheadRoutingTime::post(home, d, i, succ_i_v, departing_time_i_v, duration_succ_i_v, employee_i_v, employee_start_times_v) != ES_OK)
    home.fail();
}
