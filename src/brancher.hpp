#ifndef ROUTING_BRANCHER
#define ROUTING_BRANCHER

#include <gecode/search.hh>
#include <gecode/int.hh>

using namespace Gecode;

namespace HomeCare {
  class HomeCareModel;
}

using namespace HomeCare;

class RoutingBrancher;

class RoutingChoice : public Choice
{
public:
  enum BranchType { BRANCH_ON_SUCC = 0, BRANCH_ON_START_TIME, BRANCH_ON_EMPLOYEE };
  BranchType type;
  int day;
  int activity;
  int *values;
  int n_values;

  RoutingChoice(const RoutingBrancher& b, BranchType t, int d, int a, const std::vector<int>& vals);
  /**  */
  virtual size_t size(void) const;
  /** Add choice to archive. */
  virtual void archive(Archive& e) const;
  ~RoutingChoice();
};

/** This brancher interleaves the assignment of succ and start_time variables (in this order). 
 *  Moreover, it assigns the succ variable of employees in a round-robin way (apart of wildcard).
 *  The wildcard employee assignment is a singleton atomic choice which will include all the remaining nodes
 *  once the full routes of all the other employees have been designed.
 */

class RoutingBrancher : public Brancher
{
public:
  
  /** Checks whether there is any station yet to be assigned, stores vector of alternatives based on domains. */
  virtual bool status(const Space& home) const;
  /** Generates a choice. */
  virtual Choice* choice(Space& home);
  
  /** Retrieves a choice from the archive. */
  virtual Choice* choice(const Space&, Archive& e);
  /** Constructor. */
  RoutingBrancher(Home home);
  
  /** Copy constructor. */
  RoutingBrancher(Home home, bool share, RoutingBrancher& b);
  /** Clone support. */
  virtual Actor* copy(Space& home, bool share);
  
  /** Execute choice. */
  virtual ExecStatus commit(Space& home, const Choice& c, unsigned int a);
  /** Post brancher. */
  static void post(HomeCareModel& home);

  void print(const Space& home, const Choice&	c, unsigned int a, std::ostream& o) const;

protected:
  std::tuple<int, int, int> next_activity_to_assign(const HomeCareModel& home) const;
  ExecStatus fix_employee(HomeCareModel& b, int a, int d, int e) const;
};


/** Post function. */

void interleaving_brancher(HomeCareModel& home);
#endif
