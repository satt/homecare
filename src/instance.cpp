#include "instance.hpp"
#include <sstream>
#include <random>
#include <clocale>
#include <algorithm>

// Just for filling up randomly some data that is currently unavailable
std::uniform_real_distribution<float> latitude_distribution(45.7274, 45.80213), longitude_distribution(12.721, 12.982);
std::default_random_engine generator;

namespace HomeCare
{
  std::ostream& operator<<(std::ostream& os, const Item& e)
  {
    e.Print(os);
    return os;
  }

  Item::ItemMap Item::items;
  std::vector<Item*> Item::items_array;

  Item* Item::_Find(const std::string& id)
  {
    std::map<std::string, Item*>::iterator it = items.find(id);
    if (it == items.end())
      return NULL;
    else
      return it->second;
  }

  void Item::PrintAll(std::ostream& os)
  {
    for (ItemMap::const_iterator it = items.begin(); it != items.end(); it++)
      it->second->Print(os);
  }

  time_period null_period = time_period(ptime(date(1970,1,1)), ptime(date(1970,1,1))-minutes(1));

  time_period TimeHorizon::horizon = null_period;

  time_duration TimeHorizon::granularity = minutes(5); // TODO: this is the default time duration, it is set in the loader

  std::vector<time_period> TimeHorizon::working_periods;

  int TimeHorizon::total_timeslots;

  std::vector<int> TimeHorizon::accumulated_timeslots;

  ptime from_iso_extended_string(const std::string& inDateString) 
  { 
    // If we get passed a zero length string, then return the "not a date" value. 
    if (inDateString.empty()) 
    { 
      return not_a_date_time; 
    } 
         
    // Use the ISO extended input facet to interpret the string. 
    std::stringstream ss; 
    time_input_facet* input_facet = new time_input_facet(); 
    input_facet->set_iso_extended_format(); 
    ss.imbue(std::locale(ss.getloc(), input_facet)); 
    ss.str(inDateString); 
    ptime timeFromString; 
    ss >> timeFromString; 
         
    return timeFromString; 
  } 

  void TimeHorizon::Load(const JSON::Object& doc)
  {
    date start_date(from_iso_extended_string(doc["meta"]["start_date"].as_string()).date());
    date end_date(from_iso_extended_string(doc["meta"]["finish_date"].as_string()).date());
    horizon = time_period(ptime(start_date), ptime(end_date));
    const int number_of_days = (end_date - start_date).days();
    working_periods.resize(number_of_days, null_period);
    accumulated_timeslots.resize(number_of_days, 0);
    time_duration start_worktime = duration_from_string(doc["meta"]["time_window"]["start"].as_string());
    time_duration end_worktime = duration_from_string(doc["meta"]["time_window"]["finish"].as_string());
	
    for (int d = 0; d < number_of_days; d++)
    {
      working_periods[d] = time_period(ptime(horizon.begin() + days(d) + start_worktime), ptime(horizon.begin() + days(d) + end_worktime));
      int t = ts(working_periods[d].length());
      accumulated_timeslots[d] = (d == 0) ? t : accumulated_timeslots[d - 1] + t;
      total_timeslots += t;
    }	
		granularity = minutes(doc["meta"]["granularity"].as_int());
  }

  std::vector<int> Employee::available_employees;
  int Employee::total_available_employee_days = 0;


  void Employee::_Load(const JSON::Object& doc)
  {
    available_employees.resize(TimeHorizon::number_of_days(), 0);
  
    const JSON::Object& os = doc["operators"];
    std::vector<Activity*> activities = Activity::AllItems();
  
    for (const std::pair<std::string, JSON::Object>& o : os)
    {
      std::string employee_id = o.first;
      const JSON::Object& e = o.second;

      Employee* em = dynamic_cast<Employee*>(Employee::Create(employee_id));
      em->name = e["name"].as_string();      

      const JSON::Array& tws = e["time_windows"];
      time_duration total_regular_worktime = hours(0);
      for (int d = 0; d < TimeHorizon::number_of_days(); d++)
      {
        const JSON::Object& tw = tws[d]; 
        if (tw.size() > 0)
        {
          ptime start_tw = from_iso_extended_string(tw["start"].as_string());
          ptime end_tw = from_iso_extended_string(tw["finish"].as_string());
            
          em->daily_worktime_windows.push_back(time_period(start_tw, end_tw));
          em->regular_worktime.push_back(duration_from_string(tw["regular_worktime"].as_string()));
          total_regular_worktime += duration_from_string(tw["regular_worktime"].as_string());
          // TODO: the maximum allowed working time is exactly the length of the time window
          em->max_allowed_worktime.push_back(end_tw - start_tw);
          available_employees[d]++;
          total_available_employee_days++;
        }
        else
        {
          em->daily_worktime_windows.push_back(null_period);
          em->regular_worktime.push_back(duration_from_string("00:00:00"));
          em->max_allowed_worktime.push_back(duration_from_string("00:00:00"));
        }
      }
      // the number of working days is set between 1 and the maximum
      em->consecutive_working_days = std::make_pair(1, doc["meta"]["max_consecutive_days"].as_int());
      // the allowed overtime is set as a percentage above the total regular working time
      em->weekly_worktime = std::make_pair(duration_from_string(e["minimum_working_hours"].as_string()), duration_from_string(e["maximum_working_hours"].as_string()));
      // TODO: the location information for the employee is the center point of the geographic_area
      em->latitude = doc["meta"]["geographic_area"]["lower_right"][0].as_float() + (doc["meta"]["geographic_area"]["upper_left"][0].as_float() - doc["meta"]["geographic_area"]["lower_right"][0].as_float()) / 2.0;
      em->longitude = doc["meta"]["geographic_area"]["upper_left"][1].as_float() + (doc["meta"]["geographic_area"]["lower_right"][1].as_float() - doc["meta"]["geographic_area"]["upper_left"][1].as_float()) / 2.0;
    
      // Compatible activities
      for (Activity* a : activities)
        for (int d = 0; d < TimeHorizon::number_of_days(); d++)
          // first check only by temporal compatiblity
          if (em->daily_worktime_windows[d].intersection(a->allowed_time_window).length() >= a->duration)
            em->compatible_activities.insert(a);
      
      // then refine the set with declared incompatibilities
      const JSON::Array& incompatible_activities = e["incompatible_activities"];
      for (const JSON::Value& v : incompatible_activities)
      {
        Activity* a = static_cast<Activity*>(Activity::Find(v.as_string()));
        if (a && em->compatible_activities.find(a) != em->compatible_activities.end())
          em->compatible_activities.erase(a);        
      }           
    
      for (Activity* a : em->compatible_activities)
        a->compatible_employees.insert(em);
    }
  }

  void Employee::Print(std::ostream& os) const
  {
    os << "Employee(" << this->id << ") " << this->name << " ";
    os << this->consecutive_working_days.first << "-" << this->consecutive_working_days.second << " ";
    os << this->weekly_worktime.first << "-" << this->weekly_worktime.second << " ";
    for (int d = 0; d < TimeHorizon::horizon.length().hours() / 24; d++)
      os << this->daily_worktime_windows[d];
    os << std::endl;
  }

  std::vector<int> Activity::activities_requirements;
  int Activity::total_activities_requirements = 0;

  void Activity::_Load(const JSON::Object& doc)
  {
    activities_requirements.resize(TimeHorizon::number_of_days(), 0);		
	
    const JSON::Array& ds = doc["activities"];
    int day_number = 0;
    for (const JSON::Object& d : ds)
    {
      for (const std::pair<std::string, JSON::Object>& a : d)
      {
        std::string activity_id = a.first;
        JSON::Object activity = a.second;
            
        //time_duration start_td(duration_from_string(node.node().attribute("OraInizio").value())), end_td(duration_from_string(node.node().attribute("OraFine").value()));
        //ptime start_t(TimeHorizon::horizon.begin().date() + days(day_number), start_td), end_t(TimeHorizon::horizon.begin().date() + days(day_number), end_td);
        ptime start_t(from_iso_extended_string(activity["time_window"]["start"].as_string()));
        ptime end_t(from_iso_extended_string(activity["time_window"]["finish"].as_string()));

        Activity* ac = dynamic_cast<Activity*>(Activity::Create(activity_id));
        ac->allowed_time_window = time_period(start_t, end_t);
        ac->number_of_employees = activity["required_operators"].as_int();
        total_activities_requirements += ac->number_of_employees;
        activities_requirements[day_number] += ac->number_of_employees;
        ac->duration = minutes(activity["duration"].as_int());
        ac->latitude = activity["location"]["latitude"].as_float();
        ac->longitude = activity["location"]["longitude"].as_float();
      }
      day_number++;
    }
  }

  float distance(const Location* l1, const Location* l2)
  {  
    if (!l1 || !l2)
      throw std::logic_error("Invalid location object (for computing distance)");
    if (l1 == l2)
      return 0.0;
    const float R = 6373.0;
    const float pi = std::atan(1.0) * 4.0;
    float latA = (l1->latitude * pi) / 180.0, latB = (l2->latitude * pi) / 180.0;
    float lonA = (l1->longitude * pi) / 180.0, lonB = (l2->longitude * pi) / 180.0;
    return R * acos(sin(latA) * sin(latB) + cos(latA) * cos(latB) * cos(lonA - lonB));
  }

  time_duration traveling_time(const Location* l1, const Location* l2)
  {
    // FIXME: current hypothesis is that the traveling time between two locations is proportional to the distance (assuming a 40Km/h average speed)
    int granularity = (int)TimeHorizon::granularity.minutes();
    int real_dist = round(distance(l1, l2) * 60.0 / 40.0);
    return minutes(std::max(granularity, real_dist));
  }

  int ts(ptime t)
  {
    return ts(t - TimeHorizon::horizon.begin());
  }

  int ts(time_duration td)
  {
    return td.total_seconds() / TimeHorizon::granularity.total_seconds();
  }

  std::pair<int, int> ts(time_period tp)
  {
    return std::make_pair(ts(tp.begin()), ts(tp.end()));
  }

  ptime pt(int ts)
	{
		return TimeHorizon::horizon.begin() + (TimeHorizon::granularity * ts);
	}

  time_duration dur(int d)
  {
    return seconds(d * TimeHorizon::granularity.total_seconds());
  }

  void Activity::Print(std::ostream& os) const
  {
    os << "Activity(" << this->id << "@" << this->latitude << ", " << this->longitude << ") " << this->name << " " << this->number_of_employees << " "
      << this->allowed_time_window << this->duration << " " << this->compatible_employees.size() << std::endl;
  }

  std::vector<std::vector<Activity*> > HomeCareInput::activity_nodes;
  std::vector<int> HomeCareInput::daily_start_regular_node_index, HomeCareInput::daily_end_regular_node_index;

  void HomeCareInput::LoadInstance(const JSON::Object& doc)
  {
    Item::Clear();
    TimeHorizon::Load(doc);
    Activity::Load(doc);
    Employee::Load(doc);    
    PreprocessInstance();
  }
  
  void HomeCareInput::PreprocessInstance()
  {
    typedef std::vector<Activity*> ActivityList;
    typedef std::vector<Employee*> EmployeeList;


    ActivityList activities = Activity::AllItems();
    EmployeeList employees = Employee::AllItems();

    // Add wildcard employee
    Employee* we = dynamic_cast<Employee*>(Employee::Create("Wildcard"));
    we->name = "Wildcard employee";
    for (int d = 0; d < TimeHorizon::number_of_days(); d++)
    {
      we->daily_worktime_windows.push_back(TimeHorizon::working_periods[d]);
      we->regular_worktime.push_back(TimeHorizon::working_periods[d].length());
      we->max_allowed_worktime.push_back(TimeHorizon::working_periods[d].length());
    }
    we->consecutive_working_days = std::make_pair(0, TimeHorizon::number_of_days());
    we->weekly_worktime = std::make_pair(hours(0), hours(0));
    // TODO: the location information for the employee is as the first employee
    we->latitude = employees[0]->latitude;
    we->longitude = employees[0]->longitude;

    // All activities are compatible for the wildcard employee
    for (Activity* a : activities)
      for (int d = 0; d < TimeHorizon::number_of_days(); d++)
      {
        we->compatible_activities.insert(a);
        a->compatible_employees.insert(we);
      }

    employees = Employee::AllItems();
  
    activity_nodes.resize(TimeHorizon::number_of_days());
    daily_start_regular_node_index.resize(TimeHorizon::number_of_days());
    daily_end_regular_node_index.resize(TimeHorizon::number_of_days());

    for (int d = 0; d < TimeHorizon::number_of_days(); d++)
    {
      // create the subset of nodes, one for each day
      // the first nodes are the Activity of departing from the starting locations for each employee
      // notice that also employees not working on that day are added, so to allow uniform expression of constraints
      for (EmployeeList::const_iterator it = employees.begin(); it != employees.end(); it++)
      {
        Employee* e = *it;
        std::ostringstream oss;
        oss << "Dep: " << e->id << "(" << d << ")";
        Activity* departure = dynamic_cast<Activity*>(Activity::Create(oss.str()));
        departure->longitude = e->longitude;
        departure->latitude = e->latitude;
        departure->compatible_employees.insert(e);
        departure->allowed_time_window = e->daily_worktime_windows[d];
        departure->number_of_employees = 0;
        activity_nodes[d].push_back(departure);
      }
      // the following nodes are the activities (for that day)
      daily_start_regular_node_index[d] = activity_nodes[d].size();
      for (ActivityList::const_iterator it = activities.begin(); it != activities.end(); it++)
        // TODO: currently overnight time windows are not considered (i.e., they are assumed to lie entirely in one day)
        if ((*it)->allowed_time_window.intersects(TimeHorizon::working_periods[d]))
        {
          for (int i = 0; i < (*it)->number_of_employees; i++)
            activity_nodes[d].push_back(*it);
        }
      daily_end_regular_node_index[d] = activity_nodes[d].size();
      // the final nodes are the Activity of arriving at the final locations for each employee
      for (EmployeeList::const_iterator it = employees.begin(); it != employees.end(); it++)
      {
        Employee* e = *it;
        std::ostringstream oss;
        oss << "Arr: " << e->id << "(" << d << ")";
        Activity* arrival = dynamic_cast<Activity*>(Activity::Create(oss.str()));
        arrival->longitude = e->longitude;
        arrival->latitude = e->latitude;
        arrival->compatible_employees.insert(e);
        arrival->allowed_time_window = e->daily_worktime_windows[d];
        arrival->number_of_employees = 0;
        activity_nodes[d].push_back(arrival);
      }
    }
  }

  void Location::Print(std::ostream& os) const
  {
    os << "(" << latitude << ", " << longitude << ")";
  }
  
} // end of namespace HomeCare
