#ifndef HOMECARE_cpmodel_hpp
#define HOMECARE_cpmodel_hpp

#include "instance.hpp"
#ifdef QT_VERSION
#include "adapter.hpp"
#endif
#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "brancher.hpp"
#include "deferred_branching.hpp"
#include "lns_space.h"
#include "lns.h"

// FIXME: to remove later
#define DEBUG
#undef DEBUG

// TODO: when reengineering this code, modify the adapter for generality so that it will call some methods for self-updating the model (i.e., reverse control through a visitor pattern)
// TODO: refactor the code so that an interface for Routing problems with timewindows (plus a default smart branching strategy implemented through the merit functions and a lookahead propagator)

namespace HomeCare
{
	using namespace Gecode;
  
	class HomeCareOptions : public InstanceOptions
	{
	public:
		HomeCareOptions(const char* p) : InstanceOptions(p),
    _heuristic("-heuristic", "The heuristic to use, e.g., CP, LNS, ...", 0),
		_only_essential("-only_essential", "Do not post redundant constraints", false),
    _working_time("-enable_working_time", "Enable constraints on working time", true),
    _flexible_working_time("-flexible_working_time", "Consider soft constraints on working times", true),
    _neighborhood("-neighborhood", "Which neighborhood to use", 0)
		{
      // Heuristic
      _heuristic.add(0, "cp", "constraint programming");
      _heuristic.add(1, "lns", "constraint-based LNS");
      
      add(_heuristic);
      
      // Heuristic
      _neighborhood.add(0, "full-day", "relax full day(s)");
      _neighborhood.add(1, "two-employees", "relax one employee and one wildcard per day(s)");
      
      add(_neighborhood);
      
			add(_only_essential);
      add(_working_time);
      add(_flexible_working_time);
		}
		bool only_essential(void) const { return _only_essential.value(); }
		void only_essential(bool v) { _only_essential.value(v); }
    bool working_time(void) const { return _working_time.value(); }
    void working_time(bool v) { _working_time.value(v); }
    bool flexible_working_time(void) const { return _flexible_working_time.value(); }
    void flexible_working_time(bool v) { _flexible_working_time.value(v); }

    std::string neighborhood(void) const {
      switch(_neighborhood.value())
      {
        case 0:
          return "full-day";
        case 1:
          return "two-employees";
        default:
          throw std::logic_error("inexistent neighborhood");
      }
    }
    
    void neighborhood (const std::string& v)
    {
      if (v == "full-day")
        _neighborhood.value(0);
      else if (v == "two-employees")
        _neighborhood.value(1);
      else
        throw std::logic_error("inexistent neighborhood");
    }
    
    std::string heuristic(void) const {
      switch(_heuristic.value())
      {
        case 0:
          return "cp";
        case 1:
          return "lns";
				case 2:
					return "aco";
        default:
          throw std::logic_error("inexistent heuristic");
      }
    }
    
    void heuristic (const std::string& v)
    {
      if (v == "cp")
        _heuristic.value(0);
      else if (v == "lns")
        _heuristic.value(1);
			else if (v == "aco")
				_heuristic.value(2);
      else
        throw std::logic_error("inexistent heuristic");
    }
    
	protected:
		HomeCareOptions(const HomeCareOptions& opt)
			: InstanceOptions(opt),
    _heuristic(opt._heuristic),
    _only_essential(opt._only_essential),
    _working_time(opt._working_time),
    _flexible_working_time(opt._flexible_working_time),
		_neighborhood(opt._neighborhood)
		{}
    Driver::StringOption _heuristic;
		Driver::BoolOption _only_essential;
    Driver::BoolOption _working_time;
    Driver::BoolOption _flexible_working_time;
    Driver::StringOption _neighborhood;
		
	};
  
  typedef LNSOptions<HomeCareOptions> LNSHomeCareOptions;
	
	class HomeCareModel :  public LNSMinimizeScript, public DeferredBranchingSpace
	{
		friend class SchedulingModelAdapter;
    friend class ::RoutingBrancher;
	public:
		/** First model constructor */
		HomeCareModel(const HomeCareOptions& opt); 
		/** Copy constructor */
		HomeCareModel(bool share, HomeCareModel& s);  
		/** Clonation */
		virtual Space* copy(bool share);  
		/** Cost variable */
		virtual IntVar cost() const;    
		/** Printing */
		virtual void print(std::ostream& os = std::cout) const;
    
    
    /** LNS */
    
    /** Post a random branching, e.g. good for finding a random initial solution in LNS */
    virtual void initial_solution_branching(unsigned long int restart);
    
    /** Post a branching for LNS iteration step, the idea is that it should likely find a good solution  */
    virtual void neighborhood_branching();
    
    /** Method to generate a relaxed solution (i.e., a neighbor) from the current one (this) */
    virtual unsigned int relax(Space* neighbor, unsigned int free);
    
    /** Returns the number of relaxable variables */
    virtual unsigned int relaxable_vars() const { return 0; }
    
    /** Deferred branching */
    virtual void tree_search_branching()
    {
      initial_solution_branching(0);
    }
    
	protected:
		// debugging function
		forceinline void force_check_status()
		{
#ifdef DEBUG
			static unsigned int count = 0;
			bool s = status();
			if (!s)
			{
				std::cerr << "Failed status at: " << count << std::endl;
				throw std::logic_error("Failed status");
				GECODE_NEVER
			}
			count++;
#endif
		}
    // time and distance matrix setup
    void setup_matrices(IntArgs& distances, IntArgs& times, int d) const;
		/// Decision variables
		std::vector<IntVarArray> succ;
		std::vector<IntVarArray> employee;
		std::vector<IntVarArray> start_time;
		// other exposed variables
		IntVarArray worktime_on_day_v;
    BoolVarArray employee_works_on_v;
		std::vector<IntVarArray> duration;
		std::vector<IntVarArray> slack_time;
    std::vector<IntVarArray> open_route;
		std::vector<IntVarArray> daily_distances_per_activity;
    
		/// Cost variables
		IntVar cost_var;
    IntVar total_traveled_distance;
    IntVar total_overtime;
    IntVar total_slacktime;
    IntVarArray daily_traveled_distance;
    IntVar total_unassigned_activities;
		const HomeCareOptions& opt;

    // For branching
    IntVarArray number_of_employees_working_on_day;

	};
  
  // more succint helper functions
  bool starting_location(int a, int d);
  bool regular_activity(int a, int d);
  bool ending_location(int a, int d);
  bool wildcard(int e);
}

#endif
