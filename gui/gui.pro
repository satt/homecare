TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    SRGui \
    HomeCare

HomeCare.subdir = HomeCare
HomeCare.depends = SRGui
ProblemViewers.subdir = SRGui
