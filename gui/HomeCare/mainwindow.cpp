#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QShortcut>
#include "srmodel.h"
#include <random>
#include <QtCore>
#include "adapter.hpp"
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  SRModel* model = new SRModel(this);
  ui->combinedView->setModel(HomeCare::SchedulingModelAdapter::schedulingItemModel());
}

void MainWindow::openFile()
{
  // FIXME: uncomment
  QFileDialog fd(this, QFileDialog::tr("Open File"));
  if (fd.exec() == QDialog::Rejected)
    return;
  QString filename = fd.selectedFiles().first();
  try
  {
    //QString filename("/Users/digaspero/Documents/Development/HomeCare/sim_pos.xml");
    HomeCare::HomeCareOptions opt("HomeCare");
    opt.instance(filename.toStdString().c_str());
    HomeCare::HomeCareModel* m = new HomeCare::HomeCareModel(opt);
    HomeCare::SchedulingModelAdapter::updateModel(*m);
    delete m;
    ui->actionOpen->setDisabled(true); // FIXME: now just a single instance can be loaded
  } catch (std::exception e)
  {
    qDebug() << "Exception:" << e.what();
  }
}

MainWindow::~MainWindow()
{
  delete ui;
}
