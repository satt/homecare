#-------------------------------------------------
#
# Project created by QtCreator 2013-11-26T16:26:38
#
#-------------------------------------------------

QT += core gui widgets webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webkitwidgets

TARGET = HomeCare
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

CONFIG += link_prl c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../src/adapter.cpp \
    ../../src/instance.cpp \
    ../../src/cpmodel.cpp \
    ../../src/propagator.cpp \
    ../../src/brancher.cpp \
    ../../gecode-lns/lns.C \
    ../../gecode-lns/meta_lns.C

HEADERS  += mainwindow.h \
    ../../src/adapter.hpp \
    ../../src/instance.hpp \
    ../../src/cpmodel.hpp \
    ../../src/propagator.hpp \
    ../../src/brancher.hpp

FORMS += mainwindow.ui

LIBS += -L../SRGui -L/opt/local/lib \
    -lgecodedriver -lgecodesearch -lgecodekernel -lgecodesupport -lgecodeint -lgecodeminimodel \
    -L$$OUT_PWD/../../build/json -ljson -lstdc++.6

unix|win32: LIBS += -L$$OUT_PWD/../plugins
LIBS += -lsrviewersplugin

INCLUDEPATH += ../SRGui ../../src ../../json ../../gecode-lns /opt/local/include
DEPENDPATH += ../SRGui ../../src ../../json

#unix|macx: PRE_TARGETDEPS += ../SRGui/libsrviewersplugin.a
#win32: PRE_TARGETDEPS += ../SRGui/srviewersplugin.lib
