source("analysis.R")
connect("enea")
set.seed(1337)

extract_instance_info <- function(x)
{
    instance <- strsplit(as.character(unique(x$instance)), "_")[[1]][2]
    instance <- substr(instance, 1, nchar(instance)-5)
    x$activities <- as.numeric(strsplit(instance, "-")[[1]][1])
    x$employee_correction <- as.numeric(strsplit(instance, "-")[[1]][2])
    x$horizon <- as.numeric(strsplit(instance, "-")[[1]][3])
    x$seed <- as.numeric(strsplit(instance, "-")[[1]][4])
    return(x)
}

select_n_feasible <- function(x,n, except = c())
{
    fiz <- x[x$feasible & x$instance %ni% except,]
    return(fiz[sample(nrow(fiz),n),])
}

summarize_xp <- function(x)
{
    return(ddply(x, .(horizon, activities, employee_correction), summarize, cost = mean(cost) / 100, violations = mean(cost_unassigned), distance = (mean(cost_distance) / 3.0) / 10.0, overtime = ((mean(cost_overtime) / 40.0) * 10.0) / 60.0, slack = ((mean(cost_slack) / 40.0) * 10.0) / 60.0))
}

# feasibility experiments (also, CP)
x_cp <- getExperiments("homecare_comparison", F)
x_cp <- ddply(x_cp, .(instance), .fun = extract_instance_info)
x_cp$feasible <- x_cp$n_solutions > 0

#training_instances <- unique(ddply(x_cp, .(horizon, activities, employee_correction), .fun = select_n_feasible, 20)$instance) 
training_instances <- read.csv("training_20.txt")
#validation_instances <- unique(ddply(x_cp, .(horizon, activities, employee_correction), .fun = select_n_feasible, 10, training_instances)$instance)
validation_instances <- read.csv("validation_10.txt")

# write training instances file
#writeLines(unlist(lapply(training_instances, as.character)), "training_20.txt")

# write training instances file
#writeLines(unlist(lapply(validation_instances, as.character)), "validation_10.txt")

xp_summary <- ddply(x_cp, .(horizon, activities, employee_correction), summarize, feasible = (min(n_solutions) > 0), feasible_instances = sum(n_solutions > 0), cost = mean(cost), violations = mean(cost_unassigned))

#comparison_cp <- ddply(x_cp[x_cp$heuristic == "cp",], .(horizon, activities, employee_correction), select_n_feasible, 1)
#comparison_lns <- ddply(x_cp[x_cp$heuristic == "lns",], .(horizon, activities, employee_correction), select_n_feasible, 1)
load("comparison_cp.dat")
load("comparison_lns.dat")

